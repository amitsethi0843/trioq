buildscript {
    repositories {
        mavenLocal()
        maven { url "https://repo.grails.org/grails/core" }
    }
    dependencies {
        classpath "org.grails:grails-gradle-plugin:$grailsVersion"
        classpath "com.bertramlabs.plugins:asset-pipeline-gradle:2.11.1"
        classpath "org.grails.plugins:hibernate5:6.0.2"
    }
}

version "0.1"
group "wyreguide"

apply plugin:"eclipse"
apply plugin:"idea"
apply plugin:"war"
apply plugin:"org.grails.grails-web"
apply plugin:"org.grails.grails-gsp"
apply plugin:"asset-pipeline"

repositories {
    mavenLocal()
    maven { url "https://repo.grails.org/grails/core" }
}

dependencyManagement {
    imports {
        mavenBom "org.grails:grails-bom:$grailsVersion"
    }
    applyMavenExclusions false
}

dependencies {
    compile "org.springframework.boot:spring-boot-starter-logging"
    compile "org.springframework.boot:spring-boot-autoconfigure"
    compile "org.grails:grails-core"
    compile "org.springframework.boot:spring-boot-starter-actuator"
    provided "org.springframework.boot:spring-boot-starter-tomcat"
    compile "org.grails:grails-dependencies"
    compile "org.grails:grails-web-boot"
    compile "org.grails.plugins:cache"
    compile "org.grails.plugins:scaffolding"
    compile "org.grails.plugins:hibernate5"
    compile "org.hibernate:hibernate-core:5.1.1.Final"
    compile "org.hibernate:hibernate-ehcache:5.1.1.Final"
    compile 'org.grails.plugins:spring-security-core:3.0.3'
    compile "org.grails.plugins:spring-security-rest-gorm:2.0.0.M2"
    compile "commons-fileupload:commons-fileupload:1.3.1"
    compile 'org.grails.plugins:asynchronous-mail:2.0.0.RC7'
    compile 'org.grails.plugins:grails-aws:2.0.4'
    compile 'com.google.cloud:google-cloud-storage:1.0.1'
    console "org.grails:grails-console"
    profile "org.grails.profiles:web"
    runtime "com.bertramlabs.plugins:asset-pipeline-grails:2.11.1"
    runtime 'mysql:mysql-connector-java:5.1.36'

    testCompile "org.grails:grails-plugin-testing"
    testCompile "org.grails.plugins:geb"
    testRuntime "org.seleniumhq.selenium:selenium-htmlunit-driver:2.47.1"
    testRuntime "net.sourceforge.htmlunit:htmlunit:2.18"
    compile group: 'commons-io', name: 'commons-io', version: '2.5'


}


assets {
    minifyJs = true
    minifyCss = true
}
