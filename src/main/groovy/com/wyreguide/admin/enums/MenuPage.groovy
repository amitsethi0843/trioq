package com.wyreguide.admin.enums


enum MenuPage {
    DASHBOARD("DASHBOARD", "DashBoard", "dashboard", null),
    AGENT("AGENT","Agent","agent",null),
    VISITOR("VISITOR","Visitor","visitor",null),
    AGENTMANAGEMENT("AGENTMANAGEMENT","Manage","agent","AGENT"),
    AUTH("AUTH","Auth","adminAuth",null),
    MANAGE("MANAGE","Manage","adminAuth","AUTH")

//    MANAGE("MANAGE", "Manage", null, null)

    String code
    String displayName
    String controllerName
    String parentCode


    MenuPage(String code, String displayName, String controllerName, String parentCode) {
        this.code = code
        this.displayName = displayName
        this.controllerName = controllerName
        this.parentCode = parentCode
    }
}