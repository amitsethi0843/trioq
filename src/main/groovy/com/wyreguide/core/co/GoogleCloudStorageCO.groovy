package com.wyreguide.core.co

import com.wyreguide.common.SupportingFile
import com.wyreguide.core.enums.Simple

class GoogleCloudStorageCO implements grails.validation.Validateable  {
    byte[] fileBytes
    String username
    String contentType
    String path

    static constraints={
        fileBytes nullable:false
        username nullable:false,blank:false
        contentType nullable:false,blank:false
        path nullable:false,blank:false
    }

    GoogleCloudStorageCO(SupportingFile supportingFile,byte[] fileBytes){
        this.fileBytes=fileBytes
        this.username=supportingFile?.user?.username
        this.contentType=supportingFile.contentType
        this.path=supportingFile.path
    }
}
