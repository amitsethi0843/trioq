package com.wyreguide.core.co


class EmailCO implements grails.validation.Validateable{

    String from
    String [] to
    String [] cc
    String [] bcc
    String subject
    String htmlBody
    String content
    String replyTo
    String tag
    static constraints={
        to (nullable:false)
        from (nullable:false,blank:false)
        subject(nullable: false,blank: false)
        bcc (nullable: true)
        cc (nullable: true)
        content (nullable: true,blank:true, validator:{val,obj->
            if(!obj.htmlBody && !val){
                return "content.or.htmlBody.required"
            }
                })
        htmlBody nullable: true
        replyTo nullable: true
        tag nullable: true
    }
}
