package com.wyreguide.core.eventListeners

import com.wyreguide.Role
import com.wyreguide.User
import com.wyreguide.admin.constants.AdminConstants
import grails.transaction.Transactional
import org.springframework.context.ApplicationListener
import org.springframework.security.authentication.event.AuthenticationSuccessEvent
import org.springframework.web.context.request.RequestContextHolder


class AuthenticationSuccessEventListener implements ApplicationListener<AuthenticationSuccessEvent> {

    @Override
    @Transactional
    void onApplicationEvent(AuthenticationSuccessEvent e) {

        String username = e?.authentication?.name
        if (username) {
            User user = User.findByUsername(username)
            def session = RequestContextHolder?.currentRequestAttributes()?.session
            List<Role> roles = user.authorities;
            if (!session.getAttribute(AdminConstants.ROLES_OF_USER)) {
                session.setAttribute(AdminConstants.ROLES_OF_USER, roles)
            }
//        String targetUrl = userService.getTargetUrlForUser()
//        if(!session.getAttribute(AdminConstants.TARGET_URL)){
//            session.setAttribute(AdminConstants.TARGET_URL , targetUrl)
//        }
        }
    }
}
