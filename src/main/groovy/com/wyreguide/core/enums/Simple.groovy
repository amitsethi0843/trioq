package com.wyreguide.core.enums;


public class Simple {

    public enum UserType{
        USER("ROLE_USER"),
        ADMIN("ROLE_ADMIN")

        String userRole

        UserType(String userRole){
            this.userRole=userRole
        }
        public getKey(){
            return name()
        }
        public getRole(){
            return this.userRole
        }
    }
    public enum FileType{
        USERPROFILEIMAGE,USERRESUME,COMPANYLOGO
    }

    public enum SocialNetwork{
        LINKEDIN,FACEBOOK,BLOG,TWITTER,GITHUB,BITBUCKET
    }

}
