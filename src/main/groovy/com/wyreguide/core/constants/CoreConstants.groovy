package com.wyreguide.core.constants


interface CoreConstants {
    String ERROR_MAP = "errorMap"
    String ERROR_FLAG = "errorFlag"
    String ERROR_TEXT = "errorText"
    String ERROR_CODE = "errorCode"
}
