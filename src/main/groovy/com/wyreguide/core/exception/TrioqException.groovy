package com.wyreguide.core.exception

import com.gs.collections.impl.bimap.mutable.HashBiMap
import com.wyreguide.core.constants.CoreConstants


class TrioqException extends RuntimeException {

    TrioqException(String message) {
        super(message)
    }

    static createOrResetErrorMap(int errorFlag,String errorCode,String errorText,Map<String,Object> body){
        Map<String,Object> errorMap=new HashMap<String,Object>()
        errorMap.put(CoreConstants.ERROR_FLAG,errorFlag)
        errorMap.put(CoreConstants.ERROR_CODE,errorCode)
        errorMap.put(CoreConstants.ERROR_TEXT,errorText)
        body.put(CoreConstants.ERROR_MAP,errorMap)
    }

    static HashMap<String,Object> createBodyWithSuccessErrorMap(HashMap<String,Object> body=[:]){
        Map<String,Object> errorMap=new HashMap<String,Object>()
        errorMap.put(CoreConstants.ERROR_FLAG,0)
        errorMap.put(CoreConstants.ERROR_CODE,"000")
        errorMap.put(CoreConstants.ERROR_TEXT,"SUCCESS")
        body.put(CoreConstants.ERROR_MAP,errorMap)
        return body
    }
}
