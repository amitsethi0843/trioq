package com.wyreguide.constants

class RoleConstants {
    public static final String ROLE_ADMIN = "ROLE_ADMIN"
    public static final String ROLE_USER = "ROLE_USER"
    public static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS"
    public static final String ROLE_PREFIX ="ROLE_"
    public static final String ROLE_DASHBOARD ="ROLE_DASHBOARD"
}
