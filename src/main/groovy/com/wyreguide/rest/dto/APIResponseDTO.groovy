package com.wyreguide.rest.dto

import com.wyreguide.core.dto.ResponseDTO
import com.wyreguide.rest.constants.RestAPIConstants

class APIResponseDTO extends ResponseDTO {
    Integer statusCode = HttpURLConnection.HTTP_OK

//    TODO: Refactor/remove as needed.
    static APIResponseDTO getErrorDto(String message = RestAPIConstants.ERROR_MESSAGE) {
        new APIResponseDTO(status: false, message: message, statusCode: HttpURLConnection.HTTP_INTERNAL_ERROR)
    }

    static APIResponseDTO getNotFoundDto(String message = RestAPIConstants.ERROR_MESSAGE) {
        new APIResponseDTO(status: false, message: message, statusCode: HttpURLConnection.HTTP_NOT_FOUND)
    }
    //TODO: Refactor/remove as needed.
    static APIResponseDTO getSuccessDto(String message,Integer status=null) {
        new APIResponseDTO(status: true, statusCode: status?:HttpURLConnection.HTTP_OK, message: message)
    }

    static APIResponseDTO getNoContentDto() {
        new APIResponseDTO(status: true, statusCode: HttpURLConnection.HTTP_OK)
    }

    void setInternalServerErrorResponse(Exception e, Integer code = HttpURLConnection.HTTP_INTERNAL_ERROR) {
        this.message = e.getMessage()
        this.statusCode = code
        this.status = false
    }

    void setBadRequestErrorResponse(String message) {
        this.message = message
        this.statusCode = HttpURLConnection.HTTP_BAD_REQUEST
        this.status = false
    }
//
    void populateApiErrorResponseDTO(Exception e=null, String msg = null, Integer code = HttpURLConnection.HTTP_INTERNAL_ERROR) {
        this.message = msg ?: (e.getMessage()?:"Server Error")
        this.statusCode = HttpURLConnection.HTTP_OK
        this.code=code
        this.status = false
    }

    void populateCustomErrorDTO(String msg, Integer code) {
        this.message = msg
        this.statusCode = HttpURLConnection.HTTP_OK
        this.code=code
        this.status = false
    }

    void setSuccessCodeApiErrorResponseDTO(String message,def data=null){
        this.message = message
        this.status = true
        this.data = data
        this.statusCode=HttpURLConnection.HTTP_OK
    }

    void populateApiResponseDTO(String message, Boolean status, def data) {
        this.message = message
        this.status = status
        this.data = data
        this.statusCode=HttpURLConnection.HTTP_OK
    }

}
