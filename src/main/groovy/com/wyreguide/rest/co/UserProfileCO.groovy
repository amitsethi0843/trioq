package com.wyreguide.rest.co

class UserProfileCO implements grails.validation.Validateable{
    String firstName
    String lastName
    String totalExperience
    String aboutMe
    String line1
    String city
    String state
    String country
    String line2
    String profileSummary

    static constraints={
        firstName (nullable:false,blank:false)
        lastName (nullable:false,blank:false)
        city (nullable:false,blank:false,maxSize:15)
        state (nullable:false,blank:false,maxSize:15)
        country (nullable:false,blank:false,maxSize:15)
        totalExperience (nullable:true,blank:true)
        aboutMe (nullable:true,blank:true)
        line1 (nullable:true,blank:true)
        line2 (nullable:true,blank:true)
        profileSummary(nullable:true,blank:true)
    }
}
