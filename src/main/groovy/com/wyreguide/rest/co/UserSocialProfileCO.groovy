package com.wyreguide.rest.co

import com.wyreguide.core.enums.Simple


class UserSocialProfileCO implements grails.validation.Validateable {
    String socialAccount
    String socialAccountUrl

    static constraints={
        socialAccount(nullable:false,blank: false)
        socialAccountUrl(url:true,nullable: false,blank:false)
    }
}
