package com.wyreguide.rest.co


class RegisterUserCO implements grails.validation.Validateable{
    String username
    String password
    String retypePassword

    static constraints={
        username(nullable:false,blank:false)
        password(nullable:false,blank:false)
        retypePassword(nullable:false,blank:false,validator:{val,obj->
            if(!val.equals(obj.password)){
                return "password.doesnt.match"
            }
        })
    }

}
