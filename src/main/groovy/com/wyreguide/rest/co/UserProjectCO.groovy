package com.wyreguide.rest.co


class UserProjectCO implements grails.validation.Validateable{
    String title
    String url
    String technologiesUsed
    String description
    Boolean organizationProject
    String organization
    String months
    Boolean ongoing
    String uuid

    static constraints={
        title(nullable:false,blank:false)
        url(nullable: true,blank: true)
        technologiesUsed(nullable: true,blank: true)
        months(nullable:false,blank:false)
        description(nullable: false,blank: false)
        uuid(nullable: true,blank: true)
        organization (nullable:true,blank:true,validator:{val,obj->
            if(obj.organizationProject && !val){
                return "toDate.required.ifNot.presentCompany"
            }
        })

    }
}
