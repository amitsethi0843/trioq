package com.wyreguide.rest.co


class UserExperienceCO implements grails.validation.Validateable {
    String companyName
    String fromDate
    String tillDate
    String role
    String responsibilities
    String presentCompany
    String uuid

    static constraints={
        companyName (nullable:false,blank:false)
        fromDate (nullable:false,blank:false)
        tillDate (nullable:true,blank:true,validator:{val,obj->
            if(!obj.presentCompany && !val){
                return "toDate.required.ifNot.presentCompany"
            }
        })
        role (nullable:false,blank:false)
        presentCompany(nullable:true,blank:true)
        responsibilities(nullable:true,blank:true)
        uuid(nullable:true,blank:true)
    }
}
