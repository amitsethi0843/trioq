package com.wyreguide.rest.co


class UserEducationCO implements grails.validation.Validateable{
    String currentlyPursuing
    String fromDate
    String tillDate
    String courseName
    String university
    String uuid

    static constraints={
        courseName (nullable:false,blank:false)
        university(nullable:false,blank:false)
        fromDate (nullable:false,blank:false)
        tillDate (nullable:true,blank:true,validator:{val,obj->
            if(!obj.currentlyPursuing && !val){
                return "toDate.required.ifNot.currentlyPursuing"
            }
        })
        currentlyPursuing(nullable:true,blank:true)
        uuid(nullable:true,blank:true)
    }
}
