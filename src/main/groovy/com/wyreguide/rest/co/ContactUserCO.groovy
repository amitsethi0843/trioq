package com.wyreguide.rest.co


class ContactUserCO implements grails.validation.Validateable{
    String name
    String email
    String contactNumber
    String message
    String toEmail

    static constraints={
        name(nullable:false,blank:false)
        email(nullable:false,blank:false)
        message(nullable:false,blank:false)
        toEmail(nullable: true,blank: true)
    }

}
