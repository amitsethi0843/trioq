package com.wyreguide.rest.exception


class UserNotLogedInException extends RuntimeException{
    def UserNotLogedInException(String message) {
        super(message)
    }
}
