package com.wyreguide.rest.exception


class ValidationException extends RuntimeException{
    def ValidationException(String message) {
        super(message)
    }
}
