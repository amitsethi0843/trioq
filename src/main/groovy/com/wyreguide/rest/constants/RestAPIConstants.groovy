package com.wyreguide.rest.constants

interface RestAPIConstants {

    String CURRENCY_TYPE = 'INR'
    String CURRENCY_SYMBOL = 'Rs'
    String DISCOUNT_PERCENT = '%'
    String ERROR_MESSAGE = "Something went wrong, please try again later"

    //Aggregation Builder price range interval


    //Response JSON Constants for product listing
    String POPULARITY = 'popularity'
    String ARRIVAL = 'arrival'
    String MIN = 'min'
    String MAX = 'max'
    String OFFSET = 'offset'
    String LABEL = 'label'
    String SELECTED = 'selected'
    String NULL = 'null'

    String URL_AMPERSAND = "&"
    String URL_QUESTION_MARK = "?"
    String ACCESS_TOKEN = "access_token"
    String EMAIL = "email"
    String FACEBOOK_ID = "id"
    String NAME = 'name'
    String CLASS = 'class'
    String ERROR = 'error'
    String URL_STRING = 'url'
    String BREAD_CRUMB = 'breadCrumb'
    //COD availability


    //Login Json Constants
    String FIRST_NAME = 'firstName'
    String LAST_NAME = 'lastName'
    String GENDER = 'gender'
    String BAG_COUNT = "bagCount"
    String USER_ID = "userId"
    String IS_EXISTING_USER = "isExistingUser"
    String UTM_DATA = "utmData"

    String REDIRECT_TO_PAYMENT_GATEWAY = "redirectToPaymentGateway"
    String GOOGLE_ID = "id"
    String EMAILS = "emails"
    String ACTUAL_CATEGORY = 'actualCategory'
    String SUB_CATEGORY = 'subCategory'
    String CATEGORY_SUGGESTIONS = 'categorySuggestions'
    String DISPLAY_NAME = 'displayName'


}
