package com.wyreguide.rest.vo

class UserProfileVO{
    String firstName
    String lastName
    String username
    String uuid
    String aboutMe
    String line1
    String line2
    String city
    String state
    String country
    Long totalExperience
    UserExperienceVO currentExperience
    List<UserExperienceVO> experienceList
    List<UserEducationVO> educationList
    List<String> profileSummary
    List<UserProjectVO> userProjects
    List<UserSocialNetworkVO> userSocialNetworkVOS
    String profileSummaryString
    List skills
    String profileImageUri
    boolean hasResume=false
}
