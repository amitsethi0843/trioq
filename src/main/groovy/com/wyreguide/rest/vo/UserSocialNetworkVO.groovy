package com.wyreguide.rest.vo

import com.wyreguide.social.Social

class UserSocialNetworkVO {
    String socialNetwork
    String socialNetworkUrl

    UserSocialNetworkVO(Social social){
        this.socialNetwork=social?.socialNetwork?.toString()
        this.socialNetworkUrl=social?.profileLink
    }
}
