package com.wyreguide.rest.vo


class UserProjectVO {
    String title
    String url
    String uuid
    String technologiesUsed
    boolean organizationProject
    String company
    Long months
    boolean ongoing
    String description
}
