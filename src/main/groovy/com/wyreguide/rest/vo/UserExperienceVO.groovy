package com.wyreguide.rest.vo


class UserExperienceVO {
    String uuid
    String fromDate
    String toDate
    String position
    String responsibilities
    String companyName
}
