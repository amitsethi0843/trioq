package com.wyreguide.rest.vo

class UserEducationVO {
    String uuid
    String fromDate
    String toDate
    String courseName
    String university
}
