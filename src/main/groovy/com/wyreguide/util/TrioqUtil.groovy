package com.wyreguide.util

import com.wyreguide.ApplicationContextHolder
import com.wyreguide.rest.exception.ValidationException
import org.apache.commons.logging.LogFactory
import java.text.NumberFormat
import java.text.ParseException

class TrioqUtil {

    static def messageSource=ApplicationContextHolder.getBean("messageSource")

    public static final HTTP_NO_DATA_FOUND=210;
    private static final log = LogFactory.getLog(this)
    public static getUniqueID() {
        return UUID.randomUUID().toString().replaceAll("-", "")
    }

    public static boolean saveList(List<Objects> objects) {
        try {
            objects.each {
                if (!it.validate() && it.hasErrors() && !it.save(flush: true)) {
                    println(it.errors)
                }
            }
            return true
        }
        catch (Exception e) {
            return false
        }
    }

    static Boolean save(def object) {
        Boolean result = false
        if (object.validate() && !object.hasErrors() && object.save(flush: true)) {
            result = true
        } else {
            object.errors.allErrors.each {
                log.error( '-----------------' + it)
            }
            result = false
        }
        return result
    }

    static Object saveObject(def object) {
        Boolean result = false
        if (object.validate() && !object.hasErrors() && object.save(flush: true)) {
            result = true
        } else {
            object.errors.allErrors.each {
                log.error( '-----------------' + it)
            }
            result = false
        }
        return result ? object : null
    }

    static throwValidationException(Object obj){
        List<String> errors=[]
        obj.errors.allErrors.each{
            errors.add(messageSource.getMessage(it, null))
        }
        throw new ValidationException(errors.join(","))
    }

    static String getErrors(Object obj){
        List<String> errors=[]
        obj.errors.allErrors.each{
            errors.add(it)
        }
        return errors?errors.join(","):null
    }

    static Date strToDate(String strDate){
        strDate? new Date().parse("dd/MMM/yyyy",strDate):null
    }

    static String dateToStr(Date date){
        date? date.format("dd/MMM/yyyy"):null
    }



    public static Integer compareMonthAndYear(Date sourceDate,Date fromDate){
        Calendar source=Calendar.getInstance();
        Calendar from=Calendar.getInstance();
        source.setTime(sourceDate);
        from.setTime(fromDate);
        if(from.get(Calendar.YEAR)>source.get(Calendar.YEAR)){
            return -1;
        }
        else if(from.get(Calendar.YEAR)<source.get(Calendar.YEAR)){
            return 1;
        }
        else{
            if(from.get(Calendar.MONTH)<=source.get(Calendar.MONTH)){
                return 1;
            }
            else{
                return -1;
            }
        }
//
// from.set(Calendar.YEAR,fromDate.getYear());
// from.set(Calendar.MONTH, fromDate.getMonth());
    }
// would check the presense of key value pairs in a map and would return validation errors accordingly.
// would return null if validation is successful
    public static String validateData(Map<String,Object> inHash,List<String> params){
        List<String> errorMessages=new ArrayList<String>();
        String errorMessage=null;
        for(String param:params){
            if(!inHash.containsKey(param) || inHash.get(param)==null || "".equals(inHash.get(param).toString().trim())){
                errorMessages.add(" Missing "+param+" parameter");
            }
        }
        if(!errorMessages.isEmpty()){
            errorMessage=errorMessages.join(",")
//            StringUtils.join(errorMessages,",");
        }
        return errorMessage;
    }
// used for removing characters like ',' from number string
    public static String formatNumber(String numberStr) throws ParseException{
        NumberFormat format = NumberFormat.getNumberInstance(java.util.Locale.US);
        Number number = format.parse(numberStr);
        return number.toString();
    }
// used for validating msisdn's length
//    public static String msisdnConverter(String number,String lenght) throws Exception{
//        String formattedNumber=null;
//        if(number!=null && !number.trim().isEmpty()){
//            int numberLenght=number.length();
//            if(lenght.equals(VoltConstants.NUMBER_LENGTH_TEN) && (numberLenght==10 || numberLenght==12)){
//                formattedNumber=number.length()==10?number:number.substring(2);
//            }
//            else if(lenght.equals(VoltConstants.NUMBER_LENGTH_TWELVE) && (numberLenght==10 || numberLenght==12)){
//                formattedNumber=number.length()==12?number:"91"+number;
//            }
//            else{
//                throw new Exception("either number doesn't have 10 or 12 digits or requested digists count is not 10 or 12");
//            }
//        }
//        else{
//            throw new Exception("number is null");
//        }
//        return formattedNumber;
//    }
// used for serialization
    public static byte[] getObjectBytes(Object object) throws IOException{
        ObjectOutputStream objectOutputStream = null;
        try{
            ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
            objectOutputStream=new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(object);
            objectOutputStream.flush();
            byte[] objectBytes = byteArrayOutputStream.toByteArray();
            return objectBytes;
        }
        catch(Exception e)
        {
            System.out.println("Exception in main = " +  e);
            return null;
        }
        finally
        {
            objectOutputStream.close();
        }
    }
// used for deserialization
    public static Object getObjectFromBytes(byte[] bytes) throws IOException{
        ObjectInputStream objectInputStream = null;
        try{
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
            objectInputStream = new ObjectInputStream(byteArrayInputStream);
            Object object = objectInputStream.readObject();
            return object;
        }
        catch(Exception e)
        {
            System.out.println("Exception in main = " +  e);
            return null;
        }
        finally
        {
            objectInputStream.close();
        }
    }
// used for check if a key value pair exists in a map and also validate its type
    public static Object validateAndCheckType(Map<String,Object> inHash,String keyword,Class<?> className){
        if(inHash.get(keyword)!=null && className.isAssignableFrom(inHash.get(keyword).getClass()) ){
            try{
// return className.cast(inHash.get(keyword));
                return inHash.get(keyword);
            }
            catch (Exception e){
                return null;
            }
        }
        else
            return null;
    }
// used for creating deep copy of an object
    public static Object deepCopy(Object from, Object to) throws IOException{
        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;
        try
        {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            oos.writeObject(from);
            oos.flush();
            ByteArrayInputStream bin = new ByteArrayInputStream(bos.toByteArray());
            ois = new ObjectInputStream(bin);
            to = ois.readObject();
            return to;
        }
        catch(Exception e)
        {
            System.out.println("Exception in main = " +  e);
            return null;
        }
        finally
        {
            oos.close();
            ois.close();
        }
    }



}
