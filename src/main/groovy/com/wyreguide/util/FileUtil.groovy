package com.wyreguide.util

import com.wyreguide.User
import com.wyreguide.common.SupportingFile
import com.wyreguide.core.enums.Simple.FileType
import grails.util.GrailsUtil
import grails.web.context.ServletContextHolder

import javax.servlet.http.Part
import org.apache.commons.io.FileUtils;


class FileUtil {

    static String getFilePath(FileType fileType, User user = null) {
        String path = null
        switch (fileType) {
            case FileType.USERPROFILEIMAGE:
                if (user)
                    path = "/user/${user?.uuid}/profileImage"
                break;
            case FileType.USERRESUME:
                if (user)
                    path = "/user/${user?.uuid}/resume"
        }
        path
    }

    static String saveFile(Part part, final String dirPath, final SupportingFile supportingFile) {
        Boolean fileCreated = false;
        String finalPath = null
        InputStream filecontent = null
        String resPath = getStaticResourcesDirPath()
        try {
            File file = new File(resPath + dirPath);
            if (!file.exists())
                file.mkdirs()
            filecontent = part.getInputStream()
            finalPath = resPath+supportingFile.path + "." + supportingFile.extension?:""
            File fullPathFile = new File(finalPath)
            FileUtils.copyInputStreamToFile(filecontent, fullPathFile)
            fileCreated = true
        }
        catch (Exception e) {
            println(e.getMessage())
            fileCreated = false
        }
        finally {
            if (filecontent != null) {
                filecontent.close();
            }
        }
        return fileCreated ? finalPath : null
    }

    public static String getStaticResourcesDirPath() {
        String path = '';
//        if (GrailsUtil.developmentEnv) {
//            path = ServletContextHolder.getServletContext().getRealPath("/")
//        } else {
//            path = '/mnt/'
//        }
        path = '/mnt/wguide/'
        return path
    }

    public static String getExtension(Part part) {
        String extension=null;
        String name=part.getName()
        if (name.indexOf(".")) {
            extension=name.substring(name.indexOf(".")+1,name.length())
        }
//        else {
//            switch (part.contentType) {
//                case "image/jpeg":
//                    extension = "jpeg"
//                    break
//            }
//        }
        return extension
    }

    static String getContentType(String fileExtension){
        String contentType=null

        switch (fileExtension.toUpperCase()){
            case 'JPEG':
            case 'JPG':
                contentType='image/jpeg'
                break
            case 'PNG':
                contentType='image/png'
                break
            case 'DOCX':
                contentType='application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                break
            case 'DOC':
                contentType='application/msword'
                break
            case 'PDF':
                contentType='application/pdf'
                break

        }
        contentType
    }
}
