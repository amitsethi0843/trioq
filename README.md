What is trioq?
- Trioq is/was a small project of mine which I created primarily to learn angular 4. It is just a resume creator where you can login and update your profile summary to be reflected in a publically visible profile page whose link is generated using your email. You can even add your photo or resume(doc/pdf)

Which technologies were involved in the making of trioq?
- I used grails for the backend server deployed in tomcat container and Angular 2/4/5 for the front end. All users data was stored in mysql and the application was deployed on google compute engine and static components linke images/css was on google storage. 