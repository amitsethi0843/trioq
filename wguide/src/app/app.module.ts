import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {routing} from "./app.routing"
import { AppComponent } from './app.component';
import { LoginComponent } from './components/user/login';
import { UserRegistrationComponent } from './components/user/registerUser';
import { LandingComponent } from './components/user/landing';
import {Auth} from "./services/auth";
import {CustomEventsService} from "./services/customEvents";
import {CommonService} from "./services/commonService";
import {ToastService} from "./services/toast";
import { MaterializeModule } from 'angular2-materialize';
import {PageNotFound} from "./components/pageNotFound/pageNotFound"
import {PublicUserProfileComponent} from './components/profile/publicUserProfileComponent'
import {ManageProfileComponent} from './components/profile/manageProfileComponent'
import {ManageEducationComponent} from './components/profile/education/manageEducationComponent'
import {ManageExperienceComponent} from './components/profile/experience/manageExperienceComponent'
import {ManageProjectComponent} from './components/profile/projects/manageProjectComponent'
import {ManageSkillComponent} from './components/profile/skill/manageSkillComponent'
import {SocialComponent} from './components/profile/social/socialComponent'
import {FileComponent} from './components/directives/multipart/fileComponent'
import {CollapseOnClick} from "./components/directives/collapseOnClickDirective"
import {SelectComponent} from "./components/directives/select/select"
import { CustomFormsModule } from 'ng2-validation'
import {Capitalize} from "./components/pipes/capitalize"
import {VerifyEmailComponent} from './components/profile/verifyEmail/verifyEmailComponent'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserRegistrationComponent,
    LandingComponent,
    PublicUserProfileComponent,
    ManageProfileComponent,
    ManageExperienceComponent,
    ManageProjectComponent,
    ManageEducationComponent,
    ManageSkillComponent,
    SocialComponent,
    FileComponent,
    SelectComponent,
    VerifyEmailComponent,
    Capitalize,
    PageNotFound,
    CollapseOnClick
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    CustomFormsModule,
    HttpModule,
    routing,
    MaterializeModule
  ],
  providers: [
    Auth,
    CustomEventsService,
    CommonService,
    ToastService],
  bootstrap: [AppComponent]
})
export class AppModule { }
