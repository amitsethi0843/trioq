import {ModuleWithProviders} from "@angular/core"
import {Routes,RouterModule} from "@angular/router"
import {UserRegistrationComponent} from './components/user/registerUser';
import {LandingComponent} from './components/user/landing';
import {LoginComponent} from './components/user/login'
import {PublicUserProfileComponent} from './components/profile/publicUserProfileComponent'
import {ManageProfileComponent} from './components/profile/manageProfileComponent'
import {PageNotFound} from './components/pageNotFound/pageNotFound'
import {VerifyEmailComponent} from './components/profile/verifyEmail/verifyEmailComponent'


const appRoutes:Routes=[
  {path: '', component: LandingComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: UserRegistrationComponent},
  {path: 'manage', component: ManageProfileComponent},
  {path: 'verifyEmail',component:VerifyEmailComponent},
  {path: ':userId', component: PublicUserProfileComponent},
  {path: '**', component: PageNotFound},

];

export const routing:ModuleWithProviders=RouterModule.forRoot(appRoutes);
