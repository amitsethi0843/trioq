import {environment} from "../../environments/environment"
export class AppConstants {
  private static LocationType:any = {
    METROSTATION: ["METROSTATION", "Metro Station"],
    HOTEL: ["HOTEL", "Hotel"],
    RAILWAYSTATION: ["RAILWAYSTATION", "Railway Station"]
  };

  private static time:any = {
    hoursAmPm: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
    hours: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
    ampm: ["AM", "PM"],
    minutes: [0, 15, 30, 45]
  };

  private static socialAccounts:any={
    icon:function (account:string) {
      let icon=null;
      switch (account){
        case 'FACEBOOK':
          icon='fa fa-facebook-official';
          break;
        case 'LINKEDIN':
          icon='fa fa-linkedin';
          break;
        case 'TWITTER':
          icon='fa fa-twitter';
          break;
        case 'BITBUCKET':
          icon='fa fa-bitbucket';
          break;
        case 'GITHUB':
          icon='fa fa-github';
          break;
        case 'BLOG':
          icon='fa fa-wordpress';
          break;
      }
      return icon;
    }
  }

  private static dateInput:any = {
    date: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
    months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
    year: ["2016", "2017", "2018", "2019", "2020"]
  };

  private static googleCloud:any={
    uri:"https://storage.googleapis.com/",
    storageBucket:environment.googleCloudBucket,
    userImagePreUuidPath:"/trioq/user/",
    userImagePostUuidPath:"/profileImage/",
    userResumePostUuidPath:"/resume/",
    userImageName:environment.userImageName,
    userResumeName:environment.userResumeName,

    userImageLink:function(userUuid){
      if(userUuid){
        return this.uri+this.storageBucket+this.userImagePreUuidPath+userUuid+this.userImagePostUuidPath+this.userImageName;
      }
      return this.uri+this.storageBucket+this.userImagePreUuidPath+"default.png";
    },
    defaultUserDp:function () {
      return this.uri+this.storageBucket+this.userImagePreUuidPath+"default.png";
    },
    userResumeLink:function(userUuid){
      if(userUuid){
        return this.uri+this.storageBucket+this.userImagePreUuidPath+userUuid+this.userResumePostUuidPath+this.userResumeName;
      }
    }
  }
  //private static create_s3Url=function(url){
  //  if(url){
  //    url="http://"+environment.s3Config.bucketName+".s3.amazonaws.com"+url;
  //    return url
  //  }
  //  else{
  //    url=""
  //  }
  //  return url;
  //}


  public static _fetch():any {
    return {
      locationType: this.LocationType,
      time: this.time,
      date: this.dateInput,
      gc:this.googleCloud,
      social:this.socialAccounts
      //createS3Url:this.create_s3Url
    }
  }

}
