import { Component } from '@angular/core';

@Component({
  selector: 'pageNotFound',
  templateUrl: 'pageNotFound.html',
})
export class PageNotFound  {
}
