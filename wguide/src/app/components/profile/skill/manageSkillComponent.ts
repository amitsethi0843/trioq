import {Component, OnInit, Input, Output,EventEmitter} from "@angular/core"
import {CommonService} from "../../../services/commonService"
@Component({
  selector: 'manage-skill',
  templateUrl: 'manageSkillComponent.html',
  providers: [CommonService]
})

export class ManageSkillComponent {
  @Input()skills:any = [];
  @Output()skillsChanged=new EventEmitter<any>();
  populatedSkills:any = [];
  userSkills:any;

  constructor(private commonService:CommonService) {
  }

  skillSearch(event:any) {
    this.populatedSkills = [];
    var keyword = event.target.value;
    if (keyword.length >= 2) {
      this.commonService.setData({keyword: keyword});
      this.commonService.setNoLoader(true);
      this.commonService.setUrl("manageProfile/populateSkills");
      this.commonService.postData().subscribe(
        data=> {
          this.populatedSkills = data.data;
        }
      )
    }
  }

  saveSkills() {
    this.commonService.setData({"skills": this.userSkills});
    this.commonService.setUrl("manageProfile/skills");
    this.commonService.postData().subscribe(
      data=> {
        this.skills = data.data;
        this.skillsChanged.emit(this.skills);
      }
    )
  }

  removeSkill(uuid:string){
    this.commonService.setData({"uuid": uuid});
    this.commonService.setUrl("manageProfile/skills");
    this.commonService.deleteData().subscribe(
      data=> {
        this.skills = data.data;
        this.skillsChanged.emit(this.skills);
      }
    )
  }
}
