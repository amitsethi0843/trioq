import {Component,OnInit} from "@angular/core"
import {CommonService} from "../../services/commonService"
import {CustomEventsService} from "../../services/customEvents"
import { FormGroup,FormControl,FormBuilder,Validators } from '@angular/forms';
import {Auth} from "../../services/auth";
import { Router } from '@angular/router';
import {ProfileOption} from "../enums/enums"

@Component({
  templateUrl: 'manageProfileComponent.html',
  providers: [CommonService],
})

export class ManageProfileComponent implements OnInit {
  profileForm:FormGroup;
  errorResponse:string;
  profileMenu:string='BASICDETAILS';
  showError:boolean;
  profileData:any;
  userSkills:any=[];
  isDataPopulated:boolean=false;
  returnError:string;

  constructor(private customEventsService:CustomEventsService, private _fb:FormBuilder,
              private commonService:CommonService, private auth:Auth, private router:Router) {

    this.profileForm = this._fb.group({
      firstName: ['', [Validators.required,Validators.pattern('^[a-zA-Z]+$')]],
      lastName: ['', [Validators.required,Validators.pattern('^[a-zA-Z]+$')]],
      totalExperience: ['', [Validators.pattern('^\\d+$')]],
      city: ['', [Validators.required,Validators.pattern('^[a-z A-Z]+$'),Validators.maxLength(15)]],
      country: ['', [Validators.required,Validators.pattern('^[a-z A-Z]+$'),Validators.maxLength(15)]],
      state: ['', [Validators.required,Validators.pattern('^[a-z A-Z]+$'),Validators.maxLength(15)]],
      profileSummary:[],
      aboutMe: []
    })
  }

  ngOnInit() {
    var userToken = this.auth.getUserToken();
    if (!userToken) {
      this.router.navigate(['/login'])
    }

    this.commonService.setUrl("manageProfile/fetchProfile");
    this.commonService.getData().subscribe(
      data=> {
        this.profileData = data.data;
        this.populateFormGroup();
        if (this.profileData.skills) {
          this.userSkills = this.profileData.skills
        }
        this.isDataPopulated=true
      }
    );
  }

  populateFormGroup(){
    this.profileForm.patchValue({
      firstName:this.profileData.firstName,
      lastName:this.profileData.lastName,
      totalExperience:this.profileData.totalExperience,
      city:this.profileData.city,
      country:this.profileData.country,
      state:this.profileData.state,
      profileSummary:this.profileData.profileSummaryString,
      aboutMe:this.profileData.aboutMe
    })
  }

  saveUserDetails() {
    this.errorResponse = null;
    if (this.profileForm.valid) {
      this.showError = false;
      this.commonService.setData(this.profileForm.value);
      this.commonService.setUrl("manageProfile/basicDetails");
      this.commonService.postData().subscribe(
        data=> {
          this.profileData = data.data;
        }
      )
    }
    else {
      this.showError = true;
    }
  }

  uploadImageEvent(event) {
    let fileList:FileList = event.target.files;
    if (fileList.length > 0) {
      let file:File = fileList[0];
      let formData:FormData = new FormData();
      formData.append('uploadFile', file, file.name);
      console.log(formData)
      this.commonService.setData(formData);
      this.commonService.postData();
    }
  }

  downloadResume(){
    //this.commonService.setUrl("userProfile/resume/"+this.userId);
    //this.commonService.getMultipart()
    //  .subscribe(blob => {
    //    var link=document.createElement('a');
    //    link.href=window.URL.createObjectURL(blob);
    //    link.download=this.userId+"_resume.docx";
    //    link.click();
    //  })

  }




}
