import {Component,Input,Output,EventEmitter} from "@angular/core"
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import {CommonService} from "../../../services/commonService"
@Component({
  selector: 'manage-education',
  templateUrl: 'manageEducationComponent.html',
  providers:[CommonService]
})

export class ManageEducationComponent {
  educationForm:FormGroup;
  prensent:boolean;
  showError:boolean;
  errorMessage:string;
  @Input()qualifications:any;
  @Output() educationChanged=new EventEmitter<any>();

  customErrorMessage(message:string) {
    this.showError = true;
    this.errorMessage = message
  }

  constructor(private _fb:FormBuilder,private commonService:CommonService) {
    this.educationForm = this._fb.group({
      university: ['', [Validators.required]],
      fromDate: ['', [Validators.required]],
      courseName: ['', [Validators.required]],
      currentlyPursuing: [],
      tillDate: [],
      uuid:[]
    })
  }

  saveEducation() {
    if (this.educationForm.valid) {
      if (!this.educationForm.value.currentlyPursuing && !this.educationForm.value.tillDate) {
        this.customErrorMessage("Please enter the last day ")
      }
      else {
        this.showError = false;
        this.commonService.setData(this.educationForm.value);
        this.commonService.setUrl("manageProfile/education");
        this.commonService.postData().subscribe(
          data=> {
            this.qualifications = data.data;
            this.educationChanged.emit(this.qualifications);
          }
        )
      }
    }
    else{
      console.log(JSON.stringify(this.educationForm.value))
    }
  }

  editQualification(uuid:string){
    this.educationForm.reset();
    var editQualification;
    this.qualifications.forEach(function (qualification) {
      if (qualification.uuid == uuid) {
        editQualification = qualification
      }
    });
    this.educationForm.patchValue({university:editQualification.university,tillDate:editQualification.toDate,
      fromDate:editQualification.fromDate,courseName:editQualification.courseName,
      uuid:editQualification.uuid,currentlyPursuing:editQualification.toDate==null});
  }

  removeQualification(uuid:string){
    this.commonService.setData({"uuid": uuid});
    this.commonService.setUrl("manageProfile/education");
    this.commonService.deleteData().subscribe(
      data=> {
        this.qualifications = data.data;
        this.educationChanged.emit(this.qualifications);
      }
    )
  }

  clearForm(){
    this.educationForm.reset();
  }

}
