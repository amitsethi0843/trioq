import {Component,Input,Output,EventEmitter} from "@angular/core"
import {CommonService} from "../../../services/commonService"
import { FormGroup,FormBuilder,Validators } from '@angular/forms';


@Component({
  selector: 'manage-project',
  templateUrl: 'manageProjectComponent.html',
  providers:[CommonService]
})

export class ManageProjectComponent {
  projectForm:FormGroup;
  @Input()projects:any;
  @Output()projectsAdded=new EventEmitter<any>();
  showError:boolean;
  errorMessage:string;

  customErrorMessage(message:string) {
    this.showError = true;
    this.errorMessage = message
  }

  constructor(private _fb:FormBuilder,private commonService:CommonService) {
    this.projectForm=this._fb.group({
      title:['',Validators.required],
      months:['',[Validators.required,Validators.pattern('^\\d+$')]],
      description:['',Validators.required],
      ongoing:[],
      technologiesUsed:[],
      organizationProject:['true',[]],
      organization:[],
      url:[],
      uuid:[]
    })
  }

  saveProject(){
    if (this.projectForm.valid) {
      if (this.projectForm.value.organizationProject && !this.projectForm.value.organization) {
        this.customErrorMessage("Please fill organization's name")
      }
      else {
        this.showError = false;
        this.commonService.setData(this.projectForm.value);
        this.commonService.setUrl("manageProfile/project");
        this.commonService.postData().subscribe(
          data=> {
            this.projects = data.data;
            this.projectsAdded.emit(this.projects);
            this.projectForm.reset();

          }
        )
      }
    }
  }

  clearForm(){
    this.projectForm.reset();
  }

  editProject(uuid:string){
    this.projectForm.reset();
    let editProject;
    this.projects.forEach(function (project) {
      if (project.uuid == uuid) {
        editProject = project
      }
    });
    if(editProject){
      this.projectForm.patchValue({title:editProject.title,months:editProject.months,
        description:editProject.description,ongoing:editProject.ongoing,technologiesUsed:editProject.technologiesUsed,
        uuid:editProject.uuid,organizationProject:editProject.organizationProject,organization:editProject.company,
        url:editProject.url});
    }
  }

  removeProject(uuid:string){
    this.commonService.setData({"uuid": uuid});
    this.commonService.setUrl("manageProfile/project");
    this.commonService.deleteData().subscribe(
      data=> {
        this.projects = data.data;
        this.projectsAdded.emit(this.projects);
      }
    )
  }
}
