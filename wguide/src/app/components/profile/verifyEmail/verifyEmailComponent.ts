import {Component,OnInit} from "@angular/core"
import {CommonService} from "../../../services/commonService"
import {ActivatedRoute} from '@angular/router';

@Component({
  templateUrl: 'verifyEmailComponent.html',
  providers: [CommonService]
})
export class VerifyEmailComponent implements OnInit{

  emailVerificationSuccessful:boolean=false;
  emailVerificationFailed:boolean=false;
  message:string;
  constructor(private commonService:CommonService,private route:ActivatedRoute){}

  ngOnInit(): void {
    let verifyUuid;
    let username;
    this.route.queryParams.subscribe(params => {
      username=params.username;
      verifyUuid=params.emailVerificationUuid;
    });
    if(verifyUuid && username){
      this.commonService.setUrl("verifyEmail?username="+username+"&uuid="+verifyUuid);
      this.commonService.getData().subscribe(
        data=> {
          if(data.success){
            this.emailVerificationSuccessful=true
          }
          else{
            this.emailVerificationFailed=true;
            this.message=data.message;
          }
        },
        err=>{
          this.emailVerificationFailed=true
        }
      );
    }
  }

}
