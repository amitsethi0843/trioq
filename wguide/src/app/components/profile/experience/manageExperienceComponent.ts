import {Component,OnInit,Input,Output,EventEmitter} from "@angular/core"
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import {CommonService} from "../../../services/commonService"
@Component({
  selector: 'manage-experience',
  templateUrl: 'manageExperienceComponent.html',
  providers:[CommonService]
})

export class ManageExperienceComponent {

  experienceForm:FormGroup;
  presentCompany:boolean;
  showError:boolean;
  errorMessage:string;
  @Input()experiences:any;
  @Output()experienceChanged=new EventEmitter<any>();

  customErrorMessage(message:string) {
    this.showError = true;
    this.errorMessage = message
  }

  constructor(private _fb:FormBuilder,private commonService:CommonService) {
    this.experienceForm = this._fb.group({
      companyName: ['', [Validators.required]],
      fromDate: ['', [Validators.required]],
      role: ['', [Validators.required]],
      presentCompany: [],
      tillDate: [],
      responsibilities: [],
      uuid:[]
    })
  }

  saveExperience() {
    if (this.experienceForm.valid) {
      if (!this.experienceForm.value.presentCompany && !this.experienceForm.value.tillDate) {
        this.customErrorMessage("Please fill to date")
      }
      else {
        this.showError = false;
        this.commonService.setData(this.experienceForm.value);
        this.commonService.setUrl("manageProfile/experience");
        this.commonService.postData().subscribe(
          data=> {
            this.experiences = data.data;
            this.experienceChanged.emit(this.experiences);
          }
        )
      }
    }
    else{
      console.log(this.experienceForm)
    }
  }

  clearForm(){
    this.experienceForm.reset();
  }

  editExperience(uuid:string) {
    this.experienceForm.reset();
    var editExperience;
    this.experiences.forEach(function (experience) {
      if (experience.uuid == uuid) {
        editExperience = experience
      }
    });
    this.experienceForm.patchValue({companyName:editExperience.companyName,tillDate:editExperience.toDate,
      fromDate:editExperience.fromDate,role:editExperience.position,responsibilities:editExperience.responsibilities,
      uuid:editExperience.uuid,presentCompany:editExperience.toDate==null});
  }

  removeExperience(uuid:string){
    this.commonService.setData({"uuid": uuid});
    this.commonService.setUrl("manageProfile/experience");
    this.commonService.deleteData().subscribe(
      data=> {
        this.experiences = data.data;
        this.experienceChanged.emit(this.experiences);
      }
    )
  }

}
