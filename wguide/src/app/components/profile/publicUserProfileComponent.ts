import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CommonService} from "../../services/commonService"
import {CustomEventsService} from "../../services/customEvents"
import {AppConstants} from "../../config/constants"
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ToastService} from "../../services/toast";
@Component({
  templateUrl: 'publicUserProfileComponent.html',
  providers: [CommonService]
})

export class PublicUserProfileComponent implements OnInit {

  userId: string;
  profileData: any;
  str: String;
  isSelected: string = 'about';
  uuid: string;
  profileImage: string;
  resumeUrl: string;
  contactForm: FormGroup;
  emailSent: boolean;

  constructor(private route: ActivatedRoute, private commonService: CommonService, private toastService: ToastService,
              private customEventsService: CustomEventsService, private _fb: FormBuilder) {
    this.contactForm = this._fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      contactNumber: [],
      message: ['', Validators.required]
    })
  }

  scrollTo(fragment: string) {
    this.isSelected = fragment;
    const element = document.querySelector("#" + fragment);
    if (fragment) element.scrollIntoView(fragment)
  }

  contactUser() {
    if (this.contactForm.valid) {
      let postData=this.contactForm.value;
      postData.toEmail=this.userId;
      this.commonService.setData(postData);
      this.commonService.setUrl("userProfile/contactUser");
      console.log(this.contactForm.value)
      this.commonService.postData().subscribe(
        data => {
          this.emailSent = data.status;
          if (this.emailSent) {
            this.contactForm.reset()
          }
          this.toastService.toast(data.data, 5000, "rounded")
        })
    }
  }

  setDefaultImage(){
    this.profileImage=AppConstants._fetch().gc.defaultUserDp()
  }

  // downloadResume(){
  //   this.commonService.setUrl("userProfile/resume/"+this.uuid);
  //   this.commonService.getMultipart()
  //     .subscribe(blob => {
  //       var link=document.createElement('a');
  //       link.href=window.URL.createObjectURL(blob);
  //       link.download=this.userId+"_resume.docx";
  //       link.click();
  //     })
  //
  // }


  ngOnInit() {
    this.customEventsService.changeHeaderVisibility(false);
    this.route.params.subscribe(params => {
      this.userId = params['userId'];
    });
    if (this.userId) {
      this.commonService.setUrl("userProfile/" + this.userId);
      this.commonService.postData().subscribe(
        data => {
          if (data.status) {
            this.profileData = data.data;
            this.uuid = this.profileData.uuid;
            this.resumeUrl = AppConstants._fetch().gc.userResumeLink(this.uuid);
            this.profileImage = AppConstants._fetch().gc.userImageLink(this.uuid);
            this.str = JSON.stringify(data.data)
          }
        },
        error => {

        },
        () => console.log("finished")
      )
    }
  }

  getSocialIcon(account: string): string {
    return AppConstants._fetch().social.icon(account);
  }
}
