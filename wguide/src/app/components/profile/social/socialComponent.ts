import {Component,Input,Output,EventEmitter} from "@angular/core"
import {CommonService} from "../../../services/commonService"
import {CustomEventsService} from "../../../services/customEvents"
import {AppConstants} from "../../../config/constants"

@Component({
  selector: 'social',
  templateUrl: 'socialComponent.html',
  providers:[CommonService]
})

export class SocialComponent{

  socialAccount:string="TWITTER";
  socialAccountUrl:string;

  @Input()associatedSocialAccounts:any;
  @Output()socialAccountsChanged=new EventEmitter<any>();

  constructor(private commonService:CommonService,private customEventsService:CustomEventsService){}


  onSelectEvent(selectedSocialString:string){
    this.socialAccount=selectedSocialString
  }

  // ngOnInit(){
  //   this.commonService.setUrl("manageProfile/socialProfile");
  //   this.commonService.getData().subscribe(
  //     data=>{
  //
  //       this.associatedSocialAccounts = data.data;
  //       console.log(this.associatedSocialAccounts);
  //     }
  //   )
  // }

  addSocialProfileUrl(){
    if(this.socialAccount && this.socialAccountUrl){
      this.commonService.setData({socialAccount:this.socialAccount,socialAccountUrl:this.socialAccountUrl});
      this.commonService.setNoLoader(true);
      this.commonService.setUrl("manageProfile/socialProfile");
      this.commonService.postData().subscribe(
        data=> {
          this.associatedSocialAccounts = data.data;
          this.socialAccountsChanged.emit(this.associatedSocialAccounts);
        }
      )
    }else{
      if(!this.socialAccount){
        this.customEventsService.displayError("Please select a social network from the drop down")
      }
      else{
        this.customEventsService.displayError("Social network url can't be blank")
      }
    }
  }

  getIcon(account:string):string {
    return AppConstants._fetch().social.icon(account);
  }

}
