import {Component} from '@angular/core'
import {Input} from "@angular/core";
import { FormGroup,FormControl,FormBuilder,Validators } from '@angular/forms';
import {Auth} from "../../services/auth";
import {CommonService} from "../../services/commonService"
import { Router } from '@angular/router';
import {CustomEventsService} from "../../services/customEvents"


@Component({
  templateUrl: 'login.html',
  providers: [CommonService],
})
export class LoginComponent {

  loginForm:FormGroup;
  getData:string;
  showError:boolean;
  responseError:string;

  constructor(private auth:Auth, private _fb:FormBuilder, private commonService:CommonService,private router:Router,
              private customEvents:CustomEventsService) {
    if(auth.getUserToken()){
      this.router.navigate(['/'])
    }
    this.loginForm = this._fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(4)]]
    });
  }

  loginUser() {
    this.responseError=null;
    if (this.loginForm.valid && this.loginForm.value.username && this.loginForm.value.password) {
      this.showError = false;
      this.commonService.setData(this.loginForm.value);
      this.commonService.setUrl("login");
      //
      this.commonService.postData().subscribe(
        data=> {
          if (data.access_token && data.username) {
            this.auth.setUserData(data.access_token, data.username);
          }
        },
        error=>{
          if(error=="400"){
            this.responseError="User doesn't exist"
          }
          if(error=="401"){
            this.customEvents.displayError("User doesn't exist")
          }
        },
        ()=>console.log("finished")
      )
    }
    else {
      this.showError = true;
    }
  }
}
