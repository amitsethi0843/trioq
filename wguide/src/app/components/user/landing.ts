import { Component } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import {CommonService} from "../../services/commonService"
import {ToastService} from "../../services/toast";

@Component({
  templateUrl: 'landing.html',
})

export class LandingComponent  {
  contactForm:FormGroup;
  emailSent:boolean;

  constructor(private commonService:CommonService,private _fb:FormBuilder,private toastService: ToastService) {
    this.contactForm=this._fb.group({
      name:['',Validators.required,Validators.pattern('^[a-z A-Z]+$')],
      email:['',Validators.required],
      contactNumber:['',Validators.pattern('^\d{1,20}$')],
      message:['',Validators.required]
    })
  }
  emailToAdmin(){
    if (this.contactForm.valid) {
      this.commonService.setData(this.contactForm.value);
      this.commonService.setUrl("userProfile/contactAdmin");
      this.commonService.postData().subscribe(
        data=> {
          this.emailSent = data.status;
          if(this.emailSent){
            this.contactForm.reset()
          }
          this.toastService.toast(data.data,5000,"rounded")
        }
      )
    }
  }


}
