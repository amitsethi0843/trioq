import {Component} from "@angular/core"
import {CommonService} from "../../services/commonService"
import {Auth} from "../../services/auth"
import {AppSettings} from "../../config/appSettings"
import { FormGroup,FormControl,FormBuilder,Validators } from '@angular/forms';
import {CustomValidators} from '../../services/validators'
import { Router } from '@angular/router';

@Component({
  templateUrl: 'registerUser.html',
  providers: [CommonService],
})
export class UserRegistrationComponent {
  registerRequest:any = {
    username: null,
    password: null,
    retypePassword: null
  }
  registrationForm:FormGroup;
  token:any;
  returnedUsername:any;
  showError:boolean;
  errorResponse:string;

  constructor(private commonService:CommonService, private auth:Auth, private _fb:FormBuilder, private router:Router) {
    if(auth.getUserToken()){
      this.router.navigate(['/'])
    }
    this.registrationForm = this._fb.group({
      username: ['', [Validators.required, CustomValidators.validateEmail]],
      password: ['', [Validators.required, Validators.minLength(AppSettings.fetch().appServer.passwordLength)]],
      retypePassword: ['', [Validators.required, Validators.minLength(AppSettings.fetch().appServer.passwordLength)]]
    })
  }

  registerUser() {
    this.errorResponse=null;
    if (this.registrationForm.valid) {
      this.commonService.setData(
        this.registrationForm.value
      );
      this.commonService.setUrl('register');
      this.commonService.postData().subscribe(
        data=> {
          if (data.data.access_token && data.data.username) {
            this.auth.setUserData(data.data.access_token,data.data.username);
          }
        },
        error=>console.log(JSON.stringify(error))
      )
    }
    else {
      this.showError = true;
    }
  }

}
