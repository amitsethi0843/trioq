import {Component, OnInit, Input, ViewChild} from "@angular/core"
import {CommonService} from "../../../services/commonService"
import {ToastService} from "../../../services/toast"

@Component({
  selector: 'file',
  templateUrl: 'fileComponent.html',
  providers: [CommonService,
    ToastService]
})

export class FileComponent {
  filePath: string;
  validationError: string;
  @ViewChild('fileInput') fileInput;
  @Input() url: string;
  @Input() name: string;
  @Input() maxSize: any;
  @Input() allowedExtensions: string;

  constructor(private commonService: CommonService, private toastService: ToastService) {
  }

  fileChange(event: any) {
    let validationFailed = false;
    let fi = this.fileInput.nativeElement;
    if (fi.files && fi.files[0]) {
      let fileToUpload = fi.files[0];
      if (this.maxSize) {
        let sizeValidationFailed=this.sizeValidation(fileToUpload.size);
        sizeValidationFailed ? validationFailed=sizeValidationFailed : "";
      }
      if(this.allowedExtensions && !validationFailed){
        let extensionValidationFailed=this.extensionValidation(fileToUpload.name.split(".").pop());
        extensionValidationFailed ? validationFailed=extensionValidationFailed :"";
      }
      if (!validationFailed) {
        var input = new FormData();
        var blob = new Blob([fileToUpload], {type: fileToUpload.type});
        input.append(fileToUpload.name, blob);
        this.commonService.setUrl(this.url);
        this.commonService.postMultipart(input).subscribe(
          data => {
            if (data.status) {
              this.toastService.toast(this.name + "Uploaded Successfully", 4000);
              //this.filePath=data.data
            }
          }
        );

      }
    }
  }

  extensionValidation(fileExtension:string):boolean{

    let validationFailed = true;
    let extensionUpperCase=fileExtension.toUpperCase();

    let allowedExtensions=this.allowedExtensions.split(",");
    let allowedExtensionsLength=allowedExtensions.length;

    for(var i=0;i<allowedExtensionsLength;i++){
      if(extensionUpperCase==allowedExtensions[i].toUpperCase()){
        validationFailed=false;
        break;
      }
    }
    if(validationFailed){
      this.validationError = "Invalid file type. Valid file extensions are "+ allowedExtensions
    }
    return validationFailed;
  }
  sizeValidation(fileSize): boolean {
    let validationFailed = false;
    let fileSizeInKbs = fileSize / 1000;
    if (fileSizeInKbs > this.maxSize) {
      validationFailed = true;
      this.validationError = "Size should not be greater than " + this.maxSize + " kbs"
    }
    return validationFailed;
  }


}
