import {Directive, ElementRef, HostBinding, HostListener} from "@angular/core"
import {el} from "@angular/platform-browser/testing/src/browser_util";

@Directive({
  selector:"[validate-element]"
})
export class ValidatorDirective{

  constructor(private element:ElementRef){
    console.log(this.element)
  }

  @HostListener('document:click',['$event'])
   documentClick(elem){
    console.log('clicked',elem)
  }

  @HostListener('lick',['$event'])
  elemClick(elem){
    console.log('clicked',elem)
  }
}
