import {Component,Input,Output,EventEmitter} from "@angular/core"

@Component({
  selector: 'appSelect',
  templateUrl:'select.html'
})
export class SelectComponent {

  @Input()options:Array<string>;
  @Input()selectedVal:string;
  @Output()onSelectEvent=new EventEmitter<any>();
  selectedOption:string;

  //cities = [{'name': 'SF'}, {'name': 'NYC'}, {'name': 'Buffalo'}];
  //selectedCity = this.cities[1];

  onChange(option:string) {
    this.selectedOption=option;
    this.onSelectEvent.emit(this.selectedOption);
  }
}
