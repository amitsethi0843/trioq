import { Component,OnInit } from '@angular/core';
import { Location} from '@angular/common';

import { Router,Event as RouterEvent,NavigationStart,ActivatedRoute,
  NavigationEnd,NavigationCancel,NavigationError } from '@angular/router';
import {Auth} from "./services/auth";
import {CustomEventsService} from "./services/customEvents"
import {CommonService} from "./services/commonService"
import {CollapseOnClick} from "./components/directives/collapseOnClickDirective"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  logedInUserName:string;
  userToken:string;
  showHeader:boolean=true;
  title = 'Trioq';
  loading: boolean = false;
  displayerror:any;

  constructor(private router:Router, private auth:Auth,private customEventsService:CustomEventsService,private location:Location,
              private commonService:CommonService) {
    router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event);
    });
  }
  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.loading = true;
    }
    if (event instanceof NavigationEnd) {
      this.loading = false;
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.loading = false;
    }
    if (event instanceof NavigationError) {
      this.loading = false;
    }
  }

  ngOnInit() {
    this.auth.dataChange.subscribe(data=> {
      this.userToken = data[0];
      this.logedInUserName = data[1];
      if (this.userToken) {
        this.router.navigate(['/manage'])
      }
      else {
        this.router.navigate(['/'])
      }
    });

    this.userToken = this.auth.getUserToken();
    this.logedInUserName = this.auth.getUserName();

    if (!this.userToken && this.commonService.loginRequired(this.location.path())) {
      this.router.navigate(['/login'])
    }
    //else {
    //  this.router.navigate(['/login'])
    //}

    this.customEventsService.headerVisibilityChange.subscribe(data=>{
      this.showHeader=data
    });
    this.customEventsService.httpRequest.subscribe(data=>{
      this.loading=data
    });
    this.customEventsService.emitError.subscribe(data=>{
      this.displayerror=data;
      setTimeout(()=>{
        this.displayerror = null;
      },3000);
    });
  }

  logout() {
    this.auth.removeUserData();
  }
}
