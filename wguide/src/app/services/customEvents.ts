import {Injectable,EventEmitter} from "@angular/core";

@Injectable()
export class CustomEventsService {

  headerVisible:boolean = true;
  errorData:any;
  httpRequestStatus:boolean=false;
  headerVisibilityChange:EventEmitter<any> = new EventEmitter();
  httpRequest:EventEmitter<any> = new EventEmitter();
  emitError:EventEmitter<any> = new EventEmitter();

  displayError(error:any){
    this.errorData=error;
    this.emitError.emit(this.errorData)
  }

  changeHeaderVisibility(visible:boolean){
    this.headerVisible=visible;
    this.headerVisibilityChange.emit(this.headerVisible)
  }

  changeHttpRequestStatus(status:boolean){
    this.httpRequestStatus=status;
    this.httpRequest.emit(this.httpRequestStatus)
  }


}
