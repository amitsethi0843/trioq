export const environment = {
  production: true,
  serverUrl: "http://api.trioq.com/api/",
  refreshTokenUrl:"http://api.trioq.com/oauth/access_token",
  loginRequired:['/manage'],
  googleCloudBucket:'dappled-genre-5740',
  userImageName:'profileImage',
  userResumeName:'resume'
};
