import { WguidePage } from './app.po';

describe('wguide App', () => {
  let page: WguidePage;

  beforeEach(() => {
    page = new WguidePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
