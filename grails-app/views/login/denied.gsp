<html>

<head>
    <meta name="layout" content="${gspLayout ?: 'main'}"/>
    <title><g:message code='springSecurity.denied.title'/></title>
</head>

<body>
<div class="body">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center" style="margin-top: 50px;">
                <div class="errors"><h3><g:message code='springSecurity.denied.message'/></h3></div>
            </div>
        </div>
    </div>
</div>
</body>

</html>