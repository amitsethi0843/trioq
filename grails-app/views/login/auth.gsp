<html>
<head>
    <meta name="layout" content="main">
</head>

<body>

<section id="login">
    <div class="row">
        <div class="col-md-4 col-md-offset-4" style="margin-top: 70px">
            <div class="main-box"><header class="main-box-header clearfix">
                <h2><g:message code='springSecurity.login.header'/></h2>
            </header>

                <div class="main-box-body">
                    <helper:showAlert/>
                    <g:if test='${flash.message}'>
                        <div class="login_message">${flash.message}</div>
                    </g:if>

                    <form action="${postUrl ?: '/login/authenticate'}" method="POST" id="loginForm"
                          autocomplete="off">
                        <div class="form-group">
                            <label for="username">Email address</label>
                            <input type="email" class="form-control" name="username" id="username"
                                   placeholder="Enter email">
                        </div>


                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" id="password"
                                   placeholder="Enter Password">
                        </div>

                        <div class="form-group">
                            <div class="checkbox-nice checkbox-inline">
                                <input type="checkbox" id="remember_me" name="remember-me"
                                       <g:if test='${hasCookie}'>checked="checked"</g:if>>
                                <label for="remember_me">
                                    <g:message code='springSecurity.login.remember.me.label'/>
                                </label>
                            </div>
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn btn-success" id="submit">${message(code: 'springSecurity.login.button')}</button>
                            <div style="float: right">
                                <a style="color: white" href="${createLink(mapping: 'facebook')}" class="btn btn-facebook "><i class="fa fa-facebook"></i> </a>
                                <a style="color: white" href="${createLink(mapping: 'linkedin')}" class="btn btn-linkedin "><i class="fa fa-linkedin"></i> </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    (function () {
        document.forms['loginForm'].elements['${usernameParameter ?: 'username'}'].focus();
    })();
</script>
</body>
</html>