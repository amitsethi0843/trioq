package com.wyreguide.admin

import com.wyreguide.Role
import com.wyreguide.User
import com.wyreguide.admin.constants.AdminConstants
import com.wyreguide.util.Menu
import grails.transaction.Transactional
import org.grails.web.util.WebUtils

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpSession

@Transactional
class MenuService {



    Menu findByCode(String code){
        Menu.findByCode(code);
    }

    List<Menu> fetchAllMenuList() {
        Menu.findAllByIsDeletedAndIsActive(false, true)
    }

    boolean isRoleAssigned(String menuCode) {
        boolean isRoleAssigned = false
        Menu menu = findByCode(menuCode)
//        HttpServletRequest request = WebUtils.retrieveGrailsWebRequest().currentRequest
        HttpSession session = request?.session

        if (session && request?.isRequestedSessionIdValid()) {
            Set<Role> roles = (Set) session?.getAttribute(AdminConstants.ROLES_OF_USER)
            if (roles && menu) {
                for (Role role : roles) {
                    if (role?.menuIds?.contains(menu?.id)) {
                        isRoleAssigned = true
                    }
                }
            }
        }
        isRoleAssigned
    }
}
