package com.wyreguide.admin

import com.wyreguide.Role
import com.wyreguide.User
import com.wyreguide.UserRole
import grails.transaction.Transactional

@Transactional
class AdminService {

    def createAdmin(List<User> userList){
        userList.each{User user->
            UserRole.create(user,Role.findByAuthority("ROLE_ADMIN")?:new Role(authority: "ROLE_ADMIN").save(flush:true),true)
        }
    }
}
