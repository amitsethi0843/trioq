package com.wyreguide.rest

import com.wyreguide.Role
import com.wyreguide.User
import com.wyreguide.UserRole
import com.wyreguide.auth.UserAuthToken
import com.wyreguide.core.EMailService
import com.wyreguide.core.enums.Simple.UserType
import com.wyreguide.core.exception.TrioqException
import com.wyreguide.rest.co.RegisterUserCO
import com.wyreguide.rest.dto.APIResponseDTO
import com.wyreguide.rest.exception.ValidationException
import com.wyreguide.rest.vo.UserRegistrationVO
import com.wyreguide.util.TrioqUtil
import grails.transaction.Transactional

@Transactional
class UserService {

    def springSecurityService
    EMailService EMailService

    APIResponseDTO registerUser(RegisterUserCO registerUserCO) {
        APIResponseDTO apiResponseDTO=APIResponseDTO.getErrorDto()
        if (registerUserCO.validate()) {
            User user=User.findByUsername(registerUserCO.username)
            if(user){
                throw new ValidationException("User with this email already exists")
            }
            user=new User(registerUserCO)
            user.password=springSecurityService.encodePassword(registerUserCO.password)
            TrioqUtil.save(user)
            createUserRole(user,UserType.USER)
            UserRegistrationVO userRegistrationVO=new UserRegistrationVO(username: user?.username,access_token: generateUserToken(user.username))
            apiResponseDTO.populateApiResponseDTO("success", true, userRegistrationVO)

            EMailService.sendRegistrationEmail(user,TrioqException.createBodyWithSuccessErrorMap())
        }
            else {
            TrioqUtil.throwValidationException(registerUserCO)
        }
        apiResponseDTO
    }


    def createUserRole(User user,UserType userType){
        UserRole.create(user,Role.findByAuthority(userType.role)?:new Role(authority: userType.role).save(flush:true),true)
    }

    String generateUserToken(String username){
        UserAuthToken userAuthToken=new UserAuthToken(username:username,usertoken: TrioqUtil.uniqueID)
        TrioqUtil.save(userAuthToken)
        return userAuthToken.usertoken
    }


}
