package com.wyreguide.rest

import com.wyreguide.User
import com.wyreguide.common.Address
import com.wyreguide.common.SupportingFile
import com.wyreguide.core.EMailService
import com.wyreguide.core.GoogleCloudStorageService
import com.wyreguide.core.co.GoogleCloudStorageCO
import com.wyreguide.core.constants.CoreConstants
import com.wyreguide.core.enums.Simple
import com.wyreguide.core.exception.TrioqException
import com.wyreguide.experience.Experience
import com.wyreguide.experience.TotalExperience
import com.wyreguide.project.Project
import com.wyreguide.rest.co.ContactUserCO
import com.wyreguide.rest.co.UserExperienceCO
import com.wyreguide.rest.co.UserProfileCO
import com.wyreguide.rest.co.UserProjectCO
import com.wyreguide.rest.dto.APIResponseDTO
import com.wyreguide.rest.vo.UserExperienceVO
import com.wyreguide.rest.vo.UserProfileVO
import com.wyreguide.rest.vo.UserProjectVO
import com.wyreguide.util.TrioqUtil
import com.wyreguide.util.FileUtil
import grails.transaction.Transactional
import org.jasig.cas.client.util.CommonUtils
import org.springframework.context.MessageSource

import javax.servlet.http.Part

@Transactional
class UserProfileRestService {

    MessageSource messageSource
    EducationService educationService
    SkillService skillService
    GoogleCloudStorageService googleCloudStorageService
    SocialService socialService
    EMailService EMailService

    APIResponseDTO userProfile(String username) {
        APIResponseDTO apiResponseDTO = APIResponseDTO.getErrorDto(messageSource.getMessage('user.not.found.username', null, "User not found", Locale.default))
        if (username) {
            User user = User.findByUsername(username)
            if (user) {
                UserProfileVO userProfileVO = new UserProfileVO()
                bindUserProfile(userProfileVO, user)
                apiResponseDTO.populateApiResponseDTO("success", true, userProfileVO)
            }
        }
        apiResponseDTO
    }

    APIResponseDTO manageUserProfile(UserProfileCO userProfileCO, User user) {
        APIResponseDTO apiResponseDTO = APIResponseDTO.getErrorDto()
        if (userProfileCO.validate()) {
            user.createProfile(userProfileCO)
            TrioqUtil.save(user)
            UserProfileVO userProfileVO = new UserProfileVO()
            bindUserProfile(userProfileVO, user)
            apiResponseDTO.populateApiResponseDTO("success", true, userProfileVO)
        } else {
            apiResponseDTO.message = TrioqUtil.getErrors(userProfileCO)
        }

        apiResponseDTO
    }

    APIResponseDTO getUserProfile(User user) {
        APIResponseDTO apiResponseDTO = APIResponseDTO.getErrorDto()
        UserProfileVO userProfileVO = new UserProfileVO()
        bindUserProfile(userProfileVO, user)
        apiResponseDTO.populateApiResponseDTO("success", true, userProfileVO)
        apiResponseDTO
    }

    APIResponseDTO createUserExperience(UserExperienceCO userExperienceCO, User user) {
        APIResponseDTO apiResponseDTO = APIResponseDTO.getErrorDto()
        if (userExperienceCO.validate()) {
            TotalExperience totalExperience = user.totalExperience ?: new TotalExperience(user: user)
            Experience experience
            if (userExperienceCO.uuid) {
                experience = user.totalExperience.companyExperiences.find { it.uuid.equals(userExperienceCO.uuid) }
                experience ? experience.update(userExperienceCO) : ""
                TrioqUtil.save(experience)
            } else {
                experience = new Experience(userExperienceCO)
                totalExperience.addToCompanyExperiences(experience)
                TrioqUtil.save(totalExperience)
            }
            List<UserExperienceVO> userExperienceVOList = populateUserExperienceVO(user.totalExperience)
            apiResponseDTO.populateApiResponseDTO("success", true, userExperienceVOList)

        } else {
            TrioqUtil.throwValidationException(userExperienceCO)
        }
        apiResponseDTO
    }

    APIResponseDTO createUserProject(UserProjectCO userProjectCO,User user){
        APIResponseDTO apiResponseDTO = APIResponseDTO.getErrorDto()
        if(userProjectCO.validate()){
            Project project
            if(userProjectCO.uuid){
                project=user.projects.find{it.uuid.equals(userProjectCO.uuid)}
                if(project){
                    project.update(userProjectCO)
                    TrioqUtil.save(project)
                }
            }
            else{
                project=new Project(userProjectCO)
                user.addToProjects(project)
                TrioqUtil.save(user)
            }
            List<UserProjectVO> userProjectVOList=populatUserProjectsVO(user)
            apiResponseDTO.populateApiResponseDTO("success", true, userProjectVOList)
        }
        else {
            TrioqUtil.throwValidationException(userProjectCO)
        }
        apiResponseDTO
    }

    def manageUserExperience(User user, UserProfileCO userProfileCO) {
        TotalExperience totalExperience = user?.totalExperience ?: new TotalExperience()
        userProfileCO?.totalExperience ? totalExperience.totalMonths = Long.parseLong(userProfileCO?.totalExperience) : ""
        user.totalExperience = totalExperience
    }

    void bindUserProfile(UserProfileVO userProfileVO, User user) {
        userProfileVO.username = user?.username
        userProfileVO.firstName = user?.firstName
        userProfileVO.lastName = user?.lastName
        userProfileVO.aboutMe = user?.aboutMe
        userProfileVO.uuid = user?.uuid
        userProfileVO.totalExperience = user?.totalExperience ? user?.totalExperience?.totalMonths : null
        userProfileVO.currentExperience = getCurrentUserExperience(user?.totalExperience)
        userProfileVO.experienceList = populateUserExperienceVO(user?.totalExperience)
        userProfileVO.educationList = educationService.populateUserEducationVO(user?.totalEducation)
        userProfileVO.profileSummary = user?.profileSummaryList
        userProfileVO.userProjects = populatUserProjectsVO(user)
        userProfileVO.profileSummaryString = user?.profileSummaryList ? user?.profileSummaryList?.join(';') : null
        userProfileVO.skills = skillService.getUserSkills(user, ['name', 'uuid', 'level'])
        Address address = user.addresses.find { it.current }
        userProfileVO.city = address?.city
        userProfileVO.state = address?.state
        userProfileVO.country = address?.country
        userProfileVO.hasResume = hasResume(user)
        userProfileVO.userSocialNetworkVOS=socialService.getSocialNetworkList(user)
    }

    UserExperienceVO getCurrentUserExperience(TotalExperience totalExperience) {
        Experience experience = totalExperience?.companyExperiences?.find { it.current }
        if (experience) {
            UserExperienceVO userExperienceVO = new UserExperienceVO()
            userExperienceVO.companyName = experience?.company?.name
            userExperienceVO.fromDate = experience?.fromDate?.format("dd/MMM/yyyy")
            userExperienceVO.responsibilities = experience?.responsibilities
            userExperienceVO.position = experience?.position
            return userExperienceVO
        }
        return null

    }

    List<UserProjectVO> populatUserProjectsVO(User user) {
        if (user.projects) {
            List<UserProjectVO> userProjects = []
            user.projects.each {
                UserProjectVO userProjectVO = new UserProjectVO()
                userProjectVO.title = it.title
                userProjectVO.uuid=it?.uuid
                it?.url ? userProjectVO.url = it.url : ""
                it?.technologiesUsed ? userProjectVO.technologiesUsed = it?.technologiesUsed : ""
                it?.company ? userProjectVO.company = it?.company?.name : ""
                userProjectVO.months = it?.months
                userProjectVO.organizationProject = it.organizationProject
                userProjectVO.ongoing = it.ongoing
                it?.description ? userProjectVO.description = it.description : ""
                userProjects.add(userProjectVO)
            }
            return userProjects
        }
        return null
    }

    List<UserExperienceVO> populateUserExperienceVO(TotalExperience totalExperience) {
        List<UserExperienceVO> userExperienceVOList = []
        if (totalExperience) {
            totalExperience.companyExperiences.sort().each {
                UserExperienceVO userExperienceVO = new UserExperienceVO()
                userExperienceVO.companyName = it?.company?.name
                userExperienceVO.fromDate = TrioqUtil.dateToStr(it?.fromDate)
                it?.tillDate ? userExperienceVO.toDate = TrioqUtil.dateToStr(it?.tillDate) : ""
                userExperienceVO.responsibilities = it?.responsibilities
                userExperienceVO.position = it?.position
                userExperienceVO.uuid = it?.uuid
                userExperienceVOList << userExperienceVO
            }
            return userExperienceVOList ?: null
        }
        return null
    }

    APIResponseDTO saveProfileImage(Part file, User user) {
        APIResponseDTO apiResponseDTO = APIResponseDTO.getErrorDto()
        if (file != null) {
            String filePath = FileUtil.getFilePath(Simple.FileType.USERPROFILEIMAGE, user)
            SupportingFile supportingFile = new SupportingFile(file, filePath, Simple.FileType.USERPROFILEIMAGE, user, "profileImage")
            TrioqUtil.save(supportingFile)
            GoogleCloudStorageCO googleCloudStorageCO = new GoogleCloudStorageCO(supportingFile, file.getInputStream().getBytes());
            String validationError = googleCloudStorageService.create(googleCloudStorageCO)
            if (!validationError) {
                apiResponseDTO.populateApiResponseDTO("success", true, "Image successfully uploaded")
            } else {
                apiResponseDTO.setMessage(validationError)
            }
//  String fileUploadedTo = FileUtil.saveFile(file, filePath, supportingFile)
//            if (fileUploadedTo != null) {
//                apiResponseDTO.populateApiResponseDTO("success", true, fileUploadedTo)
//            }
        }
        apiResponseDTO
    }

    APIResponseDTO saveUserResume(Part file, User user) {
        APIResponseDTO apiResponseDTO = APIResponseDTO.getErrorDto()
        if (file != null) {
            String filePath = FileUtil.getFilePath(Simple.FileType.USERRESUME, user)
            SupportingFile supportingFile = new SupportingFile(file, filePath, Simple.FileType.USERRESUME, user, "resume")
            if (supportingFile.validate()) {
                TrioqUtil.save(supportingFile)
                GoogleCloudStorageCO googleCloudStorageCO = new GoogleCloudStorageCO(supportingFile, file.getInputStream().getBytes())
                String validationError = googleCloudStorageService.create(googleCloudStorageCO)
//            String fileUploadedTo = FileUtil.saveFile(file, filePath, supportingFile)
//            if (fileUploadedTo != null) {
//                apiResponseDTO.populateApiResponseDTO("success", true, fileUploadedTo)
//            }
                if (!validationError) {
                    apiResponseDTO.populateApiResponseDTO("success", true, "Resume successfully uploaded")
                } else {
                    apiResponseDTO.setMessage(validationError)
                }
            }
            else{
                apiResponseDTO.setMessage(supportingFile.errors.allErrors.first().toString())
            }
        }
        apiResponseDTO
    }

    APIResponseDTO removeUserExperience(User user, String uuid) {
        APIResponseDTO apiResponseDTO = APIResponseDTO.getSuccessDto("No data found to remove", TrioqUtil.HTTP_NO_DATA_FOUND)
        TotalExperience totalExperience = user?.totalExperience
        if (totalExperience && totalExperience.companyExperiences && uuid) {
            Experience userExperience = totalExperience?.companyExperiences?.find { it.uuid.equals(uuid) }
            if (userExperience) {
                totalExperience.removeFromCompanyExperiences(userExperience)
                userExperience.delete(flush: true)
                apiResponseDTO.setSuccessCodeApiErrorResponseDTO("Data successfully Removed")
            }
        }
        apiResponseDTO.data = populateUserExperienceVO(totalExperience)
        apiResponseDTO
    }

    APIResponseDTO removeUserProject(User user, String uuid) {
        APIResponseDTO apiResponseDTO = APIResponseDTO.getSuccessDto("No data found to remove", TrioqUtil.HTTP_NO_DATA_FOUND)
        if(user && uuid){
            Project project=user.projects.find{it.uuid.equals(uuid)}
            if(project){
                user.removeFromProjects(project)
                project.delete(flush: true)
                apiResponseDTO.setSuccessCodeApiErrorResponseDTO("Project successfully Removed")
            }
        }
        apiResponseDTO.data = populatUserProjectsVO(user)
        apiResponseDTO
    }

    boolean hasResume(User user) {
        boolean hasResume = false
        SupportingFile supportingFile = SupportingFile.findByUserAndFileType(user, Simple.FileType.USERRESUME)
        if (supportingFile) {
            hasResume = true
        }
        hasResume
    }

    File resume(String uuid) {
        File file = null;
        if (uuid) {
            User user = User.findByUuid(uuid)
            if (user) {
                SupportingFile supportingFile = SupportingFile.findByUserAndFileType(user, Simple.FileType.USERRESUME)
                if (supportingFile) {
                    String dirPath = FileUtil.getStaticResourcesDirPath()
                    file = new File(dirPath + supportingFile?.path + "." + supportingFile?.extension)
                }
            }
        }
        return file ?: null
    }

    APIResponseDTO contactAdmin(ContactUserCO contactAdminCO){
        APIResponseDTO apiResponseDTO = APIResponseDTO.getErrorDto()
        Map<String,Object> body=new HashMap<String,Object>()
        EMailService.sendContactUserEmail(contactAdminCO,TrioqException.createBodyWithSuccessErrorMap(body),true)
        if(body?."${CoreConstants.ERROR_MAP}"?."${CoreConstants.ERROR_FLAG}"==0){
            apiResponseDTO.populateApiResponseDTO("success",true,"Email sent successfully")
        }
        apiResponseDTO
    }

    APIResponseDTO contactUser(ContactUserCO contactAdminCO){
        APIResponseDTO apiResponseDTO = APIResponseDTO.getErrorDto()
        Map<String,Object> body=new HashMap<String,Object>()
        EMailService.sendContactUserEmail(contactAdminCO,TrioqException.createBodyWithSuccessErrorMap(body),false)
        if(body?."${CoreConstants.ERROR_MAP}"?."${CoreConstants.ERROR_FLAG}"==0){
            apiResponseDTO.populateApiResponseDTO("success",true,"Email sent successfully")
        }
        apiResponseDTO
    }

    APIResponseDTO verifyEmail(String uuid,String username){
        APIResponseDTO apiResponseDTO = APIResponseDTO.getErrorDto()
        if(username && uuid){
            User user=User.findByUsernameAndEmailVerificationUuid(username,uuid)
            if(user){
                user.emailVerified=true
                user.emailVerificationUuid=TrioqUtil.uniqueID
                TrioqUtil.save(user)
                apiResponseDTO.populateApiResponseDTO("success",true,"Email verified successfully")
            }
            else{
                apiResponseDTO.populateCustomErrorDTO("This link has expired",1065)
            }
        }else{
            apiResponseDTO.setBadRequestErrorResponse("invalid or missing parameters received in request")
        }
        apiResponseDTO
    }

}
