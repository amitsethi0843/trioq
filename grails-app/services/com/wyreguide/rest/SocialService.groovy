package com.wyreguide.rest

import com.wyreguide.User
import com.wyreguide.core.enums.Simple.SocialNetwork
import com.wyreguide.rest.co.UserSocialProfileCO
import com.wyreguide.rest.dto.APIResponseDTO
import com.wyreguide.rest.vo.UserSocialNetworkVO
import com.wyreguide.social.Social
import com.wyreguide.util.TrioqUtil
import grails.transaction.Transactional

@Transactional
class SocialService {

    APIResponseDTO retrieve(User user) {
        APIResponseDTO apiResponseDTO=APIResponseDTO.getErrorDto()
        try{
            apiResponseDTO.populateApiResponseDTO("success",true,getSocialNetworkList(user))
        }catch(Exception ex){
            log.info("Error occured while retrieving user social accounts Root Cause:"+ex.getCause()+" error message: "+ex.getMessage())
        }
        apiResponseDTO
    }

    APIResponseDTO save(UserSocialProfileCO userSocialProfileCO, User user) {
        APIResponseDTO apiResponseDTO=APIResponseDTO.getErrorDto()
        try{
            if(userSocialProfileCO.validate() && user){
                Social social=findAndAndUpdate(user,userSocialProfileCO) ?: new Social(userSocialProfileCO)
                user.addToSocialNetworks(social)
                TrioqUtil.save(user)
                apiResponseDTO.populateApiResponseDTO("success",true,getSocialNetworkList(user))
            }
            else{
                apiResponseDTO.setMessage("Error occured while saving user profile due to either user not logged in or validation failure" +userSocialProfileCO.errors.allErrors.first())
                log.info("Error occured while saving user profile due to either user not logged in or validation failure Root Cause:" +userSocialProfileCO.errors.allErrors.first())
            }
        }catch(Exception ex){
            log.info("Error occured while saving user profile Root Cause:"+ex.getCause()+" error message: "+ex.getMessage())
        }
        apiResponseDTO
    }

    Social findAndAndUpdate(User user,UserSocialProfileCO userSocialProfileCO){
        Social social=user.socialNetworks.find{it.socialNetwork==SocialNetwork."${userSocialProfileCO.socialAccount}"}
        if(social){
            social.profileLink=userSocialProfileCO.socialAccountUrl
            user.removeFromSocialNetworks(social)
        }
        social
    }

    List<UserSocialNetworkVO> getSocialNetworkList(User user){
        List<UserSocialNetworkVO> userSocialNetworkVOList=new ArrayList<UserSocialNetworkVO>()
        user.getSocialNetworks().each {
            UserSocialNetworkVO userSocialNetworkVO=new UserSocialNetworkVO(it)
            userSocialNetworkVOList << userSocialNetworkVO
        }
        userSocialNetworkVOList
    }
}
