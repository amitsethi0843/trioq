package com.wyreguide.rest

import com.wyreguide.User
import com.wyreguide.education.EducationQualification
import com.wyreguide.education.TotalEducation
import com.wyreguide.rest.co.UserEducationCO
import com.wyreguide.rest.dto.APIResponseDTO
import com.wyreguide.rest.vo.UserEducationVO
import com.wyreguide.util.TrioqUtil
import grails.transaction.Transactional

@Transactional
class EducationService {

    APIResponseDTO saveEducation(User user, UserEducationCO userEducationCO) {
        APIResponseDTO apiResponseDTO = APIResponseDTO.getErrorDto()
        if (userEducationCO.validate()) {
            TotalEducation totalEducation
            if (user?.totalEducation)
                totalEducation = user?.totalEducation
            else {
                totalEducation = new TotalEducation(user: user)
                TrioqUtil.save(totalEducation)
                user.totalEducation = totalEducation
                TrioqUtil.save(user)
            }
            EducationQualification educationQualification
            if(userEducationCO.uuid){
                educationQualification=totalEducation.qualifications.find{it.uuid.equals(userEducationCO.uuid)}
                educationQualification ? educationQualification.update(userEducationCO) : ""
                TrioqUtil.save(educationQualification)
            }else{
                educationQualification = new EducationQualification(userEducationCO)
                totalEducation.addToQualifications(educationQualification)
                TrioqUtil.save(totalEducation)
            }
            List<UserEducationVO> userEducationVOList = populateUserEducationVO(user.totalEducation)
            apiResponseDTO.populateApiResponseDTO("success", true, userEducationVOList)
        } else {
            TrioqUtil.throwValidationException(userEducationCO)
        }
        apiResponseDTO
    }

    APIResponseDTO removeEducation(User user, String uuid){
        APIResponseDTO apiResponseDTO = APIResponseDTO.getErrorDto()
        TotalEducation totalEducation=user?.totalEducation
        if(totalEducation && uuid && totalEducation.qualifications){
            EducationQualification educationQualification=totalEducation?.qualifications?.find{it.uuid.equals(uuid)}
            if(educationQualification){
                totalEducation.removeFromQualifications(educationQualification)
                educationQualification.delete(flush:true)
                apiResponseDTO.populateApiResponseDTO("Data successfully Removed",true,populateUserEducationVO(totalEducation))
            }
            else{
                apiResponseDTO.populateCustomErrorDTO("Education instance not found",1065)
            }
        }
        else{
            apiResponseDTO.setBadRequestErrorResponse("Invalid or missing parameters")
        }
        apiResponseDTO
    }

    APIResponseDTO updateEducation(User user, UserEducationCO userEducationCO) {
        APIResponseDTO apiResponseDTO = APIResponseDTO.getErrorDto("Couldn't update owing to some internal error")
        if (userEducationCO.validate()) {
            if (user && user.totalEducation && userEducationCO?.uuid) {
                TotalEducation totalEducation = user?.totalEducation
                EducationQualification educationQualification = totalEducation.qualifications.find {
                    it.uuid.equals(userEducationCO?.uuid)
                }
                if (educationQualification) {
                    educationQualification.update(userEducationCO)
                    TrioqUtil.save(educationQualification)
                    List<UserEducationVO> userEducationVOList = populateUserEducationVO(user.totalEducation)
                    apiResponseDTO.populateApiResponseDTO("success", true, userEducationVOList)
                }
            }
            else {
                apiResponseDTO.setErrorResponse()
            }
        } else {
            TrioqUtil.throwValidationException(userEducationCO)
        }
        apiResponseDTO
    }

    List<UserEducationVO> populateUserEducationVO(TotalEducation totalEducation) {
        List<UserEducationVO> userEducationVOList = []
        if (totalEducation) {
            totalEducation.qualifications.sort().each {
                UserEducationVO userEducationVO = new UserEducationVO()
                userEducationVO.university = it?.university?.name
                userEducationVO.fromDate = TrioqUtil.dateToStr(it?.fromDate)
                it?.tillDate ? userEducationVO.toDate = TrioqUtil.dateToStr(it?.tillDate) : ""
                userEducationVO.courseName = it?.courseName
                userEducationVO.uuid = it?.uuid
                userEducationVOList << userEducationVO
            }
            return userEducationVOList ?: null
        }
        return null
    }


}
