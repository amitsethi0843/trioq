package com.wyreguide.rest

import com.wyreguide.User
import com.wyreguide.UserSkill
import com.wyreguide.rest.co.UserSkillsCO
import com.wyreguide.rest.dto.APIResponseDTO
import com.wyreguide.technology.Skill
import com.wyreguide.util.TrioqUtil
import grails.transaction.Transactional

@Transactional
class SkillService {

    def populateUserSkills(User user) {

    }

    def populateSkillSuggestion(String keyword) {
        APIResponseDTO apiResponseDTO = APIResponseDTO.getNoContentDto()
        List keywords = keyword.split(",")
        keyword = keywords.last()
        if (keyword) {
            List skillsInitial = Skill.createCriteria().list() {
                'ilike'("name", "%${keyword}%")
                maxResults(5)

            }
            Map<String, String> skills = skillsInitial.collectEntries {
                ["${it.name}": "${it.uuid}"]
            }
            if (skills != null && !skills.isEmpty()) {
                apiResponseDTO.populateApiResponseDTO("success", true, skills)
            }
        }

        apiResponseDTO
    }

    def getUserSkills(User user, List<String> projections) {
        List userSkills = []
        user.skills.each {
            Map skillmap = [:]
            projections.each { projection ->
                if(projection in ['uuid','level']){
                    if (it.hasProperty(projection)) {
                        skillmap.put(projection, it."${projection}")

                    }
                }
                else {
                    if(it.skill.hasProperty(projection)) {
                        skillmap.put(projection, it.skill."${projection}")

                    }
                }
            }
            userSkills.add(skillmap)
        }
        userSkills
    }

    APIResponseDTO createUserSkills(User user, UserSkillsCO userSkillsCO) {
        APIResponseDTO apiResponseDTO = APIResponseDTO.getNoContentDto()
        try {
            List<String> skills = userSkillsCO.skills.split(',')
            if (user.skills) {
                skills.removeAll(user.skills*.skill.name)
            }
            if (!skills.isEmpty()) {
                skills.each {
                    String name
                    Long level
                    if (it.indexOf(':')) {
                            name = it.split(':')[0]
                        try {
                            level = Long.parseLong(it.split(':')[1])
                        }
                        catch (Exception e){
                            level=30
                        }
                    } else {
                        name = it
                        level=30
                    }
                    UserSkill skill = createUserSkill(user, name, level)
                }
            }
            apiResponseDTO.populateApiResponseDTO("success", true, getUserSkills(user, ['name', 'uuid','level']))
        }
        catch (Exception e) {
            apiResponseDTO.populateApiErrorResponseDTO(e)
        }
        return apiResponseDTO
    }

    APIResponseDTO removeUserSkill(String uuid,User user) {
        APIResponseDTO apiResponseDTO = APIResponseDTO.getNoContentDto()
        UserSkill userSkill = user.skills.find{it.uuid.equals(uuid)}
        if (userSkill) {
            Skill skill=userSkill.skill
            skill.usersUsingIt -= 1
            userSkill.delete()
            TrioqUtil.save(skill)
            apiResponseDTO.populateApiResponseDTO("success", true, "skill removed successfully")
        }
        apiResponseDTO.data=getUserSkills(user, ['name', 'uuid','level'])
        return apiResponseDTO
    }

    UserSkill createUserSkill(User user, String name, Long percentage) {
        if (name && user && percentage) {
            Skill skill = findOrCreateSkill(name)
            UserSkill userSkill = new UserSkill(user, skill, percentage)
            TrioqUtil.save(userSkill)
            return userSkill
        }
        return null
    }

    Skill findOrCreateSkill(String name) {
        Skill skill = Skill.findByName(name)
        if (!skill) {
            skill = new Skill(name, name)
        }
        skill.usersUsingIt += 1
        TrioqUtil.save(skill)
        return skill
    }
}
