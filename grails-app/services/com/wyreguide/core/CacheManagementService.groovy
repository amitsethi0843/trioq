package com.wyreguide.core

import com.wyreguide.User
import grails.transaction.Transactional

@Transactional
class CacheManagementService {
    private Map<String,Object> trioqCache=new HashMap<String,Object>()

    def createCache(){
        createUserCache()
    }

    def createUserCache(){
        Map<String,Object> userCache=new HashMap<String,Object>()
        List users=User.createCriteria().list {
            projections{
                property("username")
                property("uuid")
                property("firstName")
                property("lastName")
            }
        }
        if(users){
            userCache= users.collectEntries{
                List<String> userDetails=it
                [(userDetails[0]) : Arrays.asList(userDetails[1],userDetails[2],userDetails[3])]
            }
            trioqCache.put("userCache",userCache)
        }
    }

    def getUserCache(){
        return trioqCache.get("userCache")
    }
}
