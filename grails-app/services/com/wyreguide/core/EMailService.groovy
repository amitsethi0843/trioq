package com.wyreguide.core

import com.gs.collections.impl.bimap.mutable.HashBiMap
import com.wyreguide.User
import com.wyreguide.core.co.EmailCO
import com.wyreguide.core.constants.CoreConstants
import com.wyreguide.core.exception.TrioqException
import com.wyreguide.rest.co.ContactUserCO
import grails.core.GrailsApplication
import grails.gsp.PageRenderer
import grails.transaction.Transactional
import org.springframework.beans.factory.annotation.Value

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@Transactional
class EMailService {

    private static ExecutorService es=Executors.newFixedThreadPool(10)

    GrailsApplication grailsApplication

    IntegrationService integrationService


    @Value('${postmarkApi.url}')
    private String postMarkUrl

    @Value('${postmarkApi.token}')
    private String postMarkServerToken

    @Value('${trioq.supportEmail}')
    private String supportEmail

    @Value('${trioq.adminEmail}')
    private String adminEmail

    PageRenderer groovyPageRenderer


    def sendRegistrationEmail(User user, Map<String,Object> body){
        if(user){
            EmailCO emailCO=new EmailCO()
            emailCO.from=this.supportEmail
            emailCO.to=[user?.username]
            emailCO.subject='Welcome to trioq'
            Map<String,String> emailBodyModel=new HashBiMap<String,String>()
            emailBodyModel.username=user?.username
            emailBodyModel.emailVerifcationUrl=grailsApplication.config.get("frontEnd.serverUrl")+"/verifyEmail?username="+user?.username+"&emailVerificationUuid="+user.emailVerificationUuid

            emailCO.htmlBody=groovyPageRenderer.render(template: '/email/user/userRegistration',model: [emailBodyModel:emailBodyModel])
            send(emailCO,body)
        }
        else{
            log.error("Error while sending registration email to the user Root Cause: user is null")
            TrioqException.createOrResetErrorMap(1,"111","Error while sending registration email to the user Root Cause: user is null",body)
        }
    }

    def sendContactUserEmail(ContactUserCO contactAdminCO, Map<String,Object> body,boolean toAdmin){
        if(contactAdminCO.validate()){
            EmailCO emailCO=new EmailCO()
            try{
                if((!toAdmin && contactAdminCO?.toEmail) || toAdmin){
                    emailCO.from=this.supportEmail
                    emailCO.to=[toAdmin?"amit@trioq.com":contactAdminCO?.toEmail]
                    emailCO.subject=contactAdminCO?.email+' is trying to contact you'
                    Map<String,String> emailBodyModel=new HashBiMap<String,String>()
                    emailBodyModel.username=contactAdminCO?.email
                    emailBodyModel.contact=contactAdminCO?.contactNumber
                    emailBodyModel.message=contactAdminCO?.message
                    emailBodyModel.name=contactAdminCO?.name
                    emailBodyModel.toEmail=toAdmin?"amit@trioq.com":contactAdminCO?.toEmail
                    emailCO.htmlBody=groovyPageRenderer.render(template: '/email/user/contactAdmin',model: [emailBodyModel:emailBodyModel])
                    send(emailCO,body)
                }
                else{
                    log.error("Error while sending contact admin email Root Cause: Couldn't send email as recipient's email id is missing" )
                    TrioqException.createOrResetErrorMap(1,"111","Couldn't send email as recipient's email id is missing",body)
                }
            }
            catch (Exception e){
                log.error("Error while sending contact admin email Root Cause: " + e.getCause() + " message: " + e.getMessage() )
                TrioqException.createOrResetErrorMap(1,"111",e.getMessage(),body)
            }
        }else{
            TrioqException.createOrResetErrorMap(1,"111",contactAdminCO.errors.allErrors.first().toString(),body)
        }
    }

    def send(EmailCO emailCO,Map<String,Object> body) {
        if(emailCO.validate() && body?."${CoreConstants.ERROR_MAP}"?."${CoreConstants.ERROR_FLAG}"==0){
            Map<String,Object> httpBody=new HashMap<String,Object>()
            Map<String,Object> httpHeaders=new HashMap<String,Object>()
            httpHeaders.put("Content-Type","application/json")
            httpHeaders.put("Accept", "application/json")
            httpHeaders.put("X-Postmark-Server-Token", this.postMarkServerToken)
            setMailHttpBody(emailCO,httpBody)
            es.execute(new Runnable() {
                @Override
                void run() {
                    integrationService.postRequest("https://api.postmarkapp.com/email",httpHeaders,httpBody)
                }
            })
        }
        else{
            TrioqException.createOrResetErrorMap(1,"111",emailCO.errors.allErrors.first().toString(),body)
        }
    }

    def setMailHttpBody(EmailCO emailCO, HashMap<String, Object> body) {
            body.From = emailCO.from
            body.To = emailCO.to.join(",")
            emailCO?.cc ? body.Cc = emailCO?.cc?.join(",") : ""
            emailCO?.bcc ? body.Bcc = emailCO?.bcc?.join(",") : ""
            body.Subject = emailCO?.subject
            emailCO?.tag ? body.Tag = emailCO?.tag?.join(",") : ""
            emailCO?.htmlBody ? body.HtmlBody = emailCO?.htmlBody : ""
            emailCO.content ? body.TextBody = emailCO?.content : ""
            emailCO.replyTo ? body.ReplyTo = emailCO?.replyTo : ""
    }
}
