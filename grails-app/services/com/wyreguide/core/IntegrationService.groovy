package com.wyreguide.core

import grails.converters.JSON
import grails.transaction.Transactional
import groovy.json.JsonOutput
import org.grails.web.json.JSONElement

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@Transactional
class IntegrationService {



    static def getRequest() {

    }

    JSONElement postRequest(String siteUrl,Map<String,Object> httpHeaders,Map<String,Object> httpBody){
        String jsonString=null
        if(siteUrl){
            URL url = new URL(siteUrl)
            HttpURLConnection con = (HttpURLConnection) url.openConnection()
            con.setRequestMethod("POST")
            if(httpHeaders){
                httpHeaders.keySet().each {
                    con.setRequestProperty(it,httpHeaders.get(it).toString())
                }
            }
            if(httpBody){
                jsonString = JsonOutput.toJson(httpBody)
            }
            con.setDoOutput(true)
            DataOutputStream wr = new DataOutputStream(con.getOutputStream())
            wr.writeBytes(jsonString)
            wr.flush()
            wr.close()
            int responseCode = con.getResponseCode()
            log.info("Sending 'POST' request to URL : " + url)
            log.info("Post Data : " + jsonString)
            log.info("Response Code : " + responseCode)
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String output
            StringBuffer response = new StringBuffer()

            while ((output = br.readLine()) != null) {
                response.append(output)
            }
            br.close()
            return JSON.parse(response.toString())
        }
    }
}
