package com.wyreguide.core

import com.wyreguide.core.co.GoogleCloudStorageCO
import grails.transaction.Transactional

import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.cloud.storage.BlobId
import com.google.cloud.storage.BlobInfo
import com.google.cloud.storage.Blob
import org.springframework.beans.factory.annotation.Value;

@Transactional
class GoogleCloudStorageService {


    Storage storage = StorageOptions.getDefaultInstance().getService()

    @Value('${google.storage.path}')
    private String path

    @Value('${google.cloud.bucket}')
    private String bucket

    def create(GoogleCloudStorageCO googleCloudStorageCO) {
        String validationMessage= null
        if (googleCloudStorageCO.validate()) {
            try {
                BlobId blobId = BlobId.of(bucket, this.path + googleCloudStorageCO.path);
                BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType(googleCloudStorageCO.contentType).build();
                Blob blob = storage.create(blobInfo, googleCloudStorageCO.fileBytes);
            }
            catch (Exception e){
                validationMessage=e.getMessage()
            }
        }else{
            validationMessage=googleCloudStorageCO.errors.allErrors
        }

        validationMessage

    }
}
