package com.wyreguide

import com.wyreguide.admin.AdminService
import com.wyreguide.admin.MenuService
import com.wyreguide.admin.enums.MenuPage
import com.wyreguide.common.Address
import com.wyreguide.company.Company
import com.wyreguide.education.EducationQualification
import com.wyreguide.education.TotalEducation
import com.wyreguide.experience.Experience
import com.wyreguide.experience.TotalExperience
import com.wyreguide.project.Project
import com.wyreguide.rest.SkillService
import com.wyreguide.technology.Skill
import com.wyreguide.util.TrioqUtil
import com.wyreguide.util.Menu
import grails.core.GrailsApplication
import grails.transaction.Transactional

import static com.wyreguide.constants.RoleConstants.ROLE_ADMIN

@Transactional
class BootstrapService {

    MenuService menuService
    def springSecurityService
    GrailsApplication grailsApplication
    AdminService adminService
    SkillService skillService

    void createAdmin() {
        User amit = new User("amitsethi0843@gmail.com", "1234", "Amit", "Sethi")
        amit.addToAddresses(new Address(city: 'New Delhi', state: 'Delhi', country: 'India', current: true))
        createProfileSummary(amit)

        TrioqUtil.save(amit)
        User admin = new User("admin@trioq.com", "1234")
        TrioqUtil.save(admin)
        adminService.createAdmin([amit, admin])
    }

    void createProjects() {
        User user = User.findByUsername("amitsethi0843@gmail.com")
        Company nexthoughts=Company.findByName("NexThoughts");
        if (user && (!user.projects || user.projects.size() <= 0)) {

            Project wyreList = new Project("wyrelist", false,3,true, "www.wyrelist.com", null, "Event Management, Event Listing, Buying and selling entry passes",
                    "Python, Django,  MySQL, Angular2, Typescript,  JQuery, HTML, CSS, Amazon EC2, GIT, Bitbucket, Twitter  Bootstrap, Amazon s3, Gunicorn, Asana, Ubuntu/OSX, Supervisor")
            user.addToProjects(wyreList)
            if(nexthoughts){
                Project bakeryBite=new Project("BakeryBite",true,9,false,"bakerybite.com",nexthoughts,"Provides large variety of bakery products to the users which they can order on specific " +
                        "time or date using multiple delivery options.It is also helpful for bakeries who want to make profit through e-commerce traffic by enlisting their products online",
                        "Grails Framework, Groovy, Java, MySQL, Mongo DB,  JQuery, Ajax, HTML, CSS, Amazon EC2, Amazon S3, GIT, Bit Bucket, Asana, Twitter Bootstrap, Facebook graph api, Google maps api, Google plus api"
                )
                user.addToProjects(bakeryBite)
                Project transferGuru=new Project("TransferGuru",true,7,false,"transferguru.org",nexthoughts,"Application that compares live rates across a variety of services for international money transfer with aim " +
                        "to bring transparency and clarity to the market.","Grails, Spring, hibernate Frameworks, Groovy, Java, Selenium/Geb/PhantomJs for scraping data from sites, MySQL,  JQuery, Ajax, HTML, CSS, Digital Ocean for Hosting, GIT, Github, Twitter Bootstrap")
                user.addToProjects(transferGuru)
                Project investobazaar=new Project("investobazaar",true,7,false,"investobazaar.com",nexthoughts,"Applications to help people out with their saving by providing comparisons between \tvarious savings plans, mutual funds, fixed deposits and so on."
                        ,"Python, Django, Celery, Selenium, phantomJS,  MySQL, AngularJS, JQuery, \tHTML, CSS, Amazon EC2, GIT, Bitbucket, Twitter  Bootstrap, Amazon s3, Gunicorn, Asana")
                user.addToProjects(investobazaar)
                Project p2p=new Project("p2p",true,4,false,"p2p.fintechlabs.com",nexthoughts,"Application would enable users to apply for loan and multiple lenders could bid on the loan based on the basis of amount, interest and tenure. " +
                        "Bid with the best rate of interest (minimum) would be selected for the fulfillment of loan. SAAS distribution model would be followed with which application would be provided on lease to various clients",
                "Grails 3, Groovy, Java, MySQL, JQuery, Ajax, HTML, CSS, Amazon AWS for Hosting, GIT, Bitbucket, Twitter and Bootstrap")
                user.addToProjects(p2p)

            }
        }
    }

    void createProfileSummary(User user) {
        if (!user.profileSummaryList || user.profileSummaryList.size() <= 0) {
            user.profileSummaryList.add("MCA with more than 3 years of experience in Full Stack Development; currently associated with  American Swan, Gurgaon as Full Stack Web Developer")
            user.profileSummaryList.add("Proficient in end-to-end development of software products from requirement analysis to system study, designing, coding, testing, documentation and implementation")
            user.profileSummaryList.add("Possess knowledge of Java, Spring, Hibernate, Grails, Groovy, Python, Django, JQuery, AngularJS, Angular 2, Git , AWS and so on.")
            user.profileSummaryList.add("Strong Object Oriented concepts for web designing and database programming and implementation")
            user.profileSummaryList.add("Proven interpersonal, communication and presentation skills, with the capability to work under pressure")
        }
    }

    void insertMenu() {

        String parentMenu = ""
        Map<String, Menu> menuMap = [:]
        MenuPage.values()?.each { MenuPage menuPage ->
            String mapKey = menuPage.code
            Menu existingMenu = menuService.findByCode(menuPage.code)
            Menu menu
            if (!existingMenu) {
                menu = new Menu(displayName: menuPage.displayName, code: menuPage.code,
                        menuItemIds: [], url: menuPage.controllerName, uuid: TrioqUtil.uniqueID)

            } else {
                menu = existingMenu.updateMenu(menuPage.displayName, menuPage.code, menuPage.controllerName)

            }
            parentMenu = menuPage.parentCode
            if (parentMenu) {
                if (menuMap.containsKey(parentMenu) || menuService.findByCode(parentMenu)) {
                    Menu menuParent = menuMap[parentMenu] ?: menuService.findByCode(parentMenu)
                    menuParent.addToMenuItems(menu)
                    menuParent.uuid = TrioqUtil.uniqueID;
                    menu.parentMenu = menuParent
                    menuMap[parentMenu] = menuParent
                }
            }

            menuMap[mapKey] = menu

        }
        List<Menu> menuList = menuMap.values().collect()

        if (menuList) {
            menuList.each {

                if (!it.save()) {
                    println(it.getErrors().allErrors.each {
                        println("--------------error" + it)
                    })
                }
            }
        }

    }


    void createRequestMap() {

        findOrEditRequestMap(grailsApplication.config.grails.admin.console.interceptUrl, [ROLE_ADMIN])
        findOrEditRequestMap(grailsApplication.config.grails.admin.console.plugin.interceptUrl, [ROLE_ADMIN])
        findOrEditRequestMap(grailsApplication.config.grails.authenticated.adminHome.interceptUrl, ['IS_AUTHENTICATED_FULLY'])
        for (String url in (grailsApplication.config.grails.default.interceptUrls as List)) {
            if (!Requestmap.findByUrl(url)) {
                new Requestmap(url: url, configAttribute: 'permitAll', uuid: TrioqUtil.uniqueID).save(flush: true)
            }
        }

        List<Menu> menuList = menuService.fetchAllMenuList()
        Requestmap requestmap
        menuList.each { Menu menu ->
            if (menu.url) {
                String url = "/${menu?.url}/**"
                requestmap = Requestmap.findByUrl(url)
                List<String> roleNameList = []
                if (!requestmap) {
                    requestmap = new Requestmap(url: url, uuid: TrioqUtil.uniqueID)

                } else {
                    roleNameList = requestmap?.configAttribute?.split("\\s*,\\s*")?.toList();
                }

                if (!(roleNameList?.contains(ROLE_ADMIN))) {
                    roleNameList.add(ROLE_ADMIN)
                    addMenuIdToRole(ROLE_ADMIN, menu?.uuid)
                }

                requestmap.configAttribute = roleNameList.join(",")
                requestmap.save(flush: true)
            }
        }
        springSecurityService.clearCachedRequestmaps()

    }

    void createTechnology() {
        User user = User.findByUsername("amitsethi0843@gmail.com")
        if (user) {
            Skill.findByName("groovy") ?: createTechnologyInstance("groovy", "best tech", user,90)
            Skill.findByName("grails") ?: createTechnologyInstance("grails", "best tech", user,100)
            Skill.findByName("python") ?: createTechnologyInstance("python", "best tech", user,50)
            Skill.findByName("django") ?: createTechnologyInstance("django", "best tech", user,60)
            Skill.findByName("spring mvc") ?: createTechnologyInstance("spring mvc", "best tech", user,60)
            Skill.findByName("java") ?: createTechnologyInstance("java", "best tech", user,80)
            Skill.findByName("angularJs") ?: createTechnologyInstance("angularJs", "best tech", user,50)
            Skill.findByName("angular 2") ?: createTechnologyInstance("angular 2", "best tech", user,90)
            Skill.findByName("jQuery") ?: createTechnologyInstance("jQuery", "best tech", user,60)
            Skill.findByName("Amazon AWS") ?: createTechnologyInstance("Amazon AWS", "best tech", user,40)
            Skill.findByName("git") ?: createTechnologyInstance("git", "best tech", user,90)
            Skill.findByName("sql databases") ?: createTechnologyInstance("sql databases", "best tech", user,70)
            Skill.findByName("mongodb") ?: createTechnologyInstance("mongodb", "best tech", user,40)
        }
    }

    void createTechnologyInstance(String name, String desc, User user,Long percentage) {
        skillService.createUserSkill(user,name,percentage)
    }

    void createQualification() {
        User user = User.findByUsername("amitsethi0843@gmail.com")
        if (user && user.totalEducation) {
            TotalEducation totalEducation = user.totalEducation
            createEducationQualification().each {
                totalEducation.addToQualifications(it)
            }
            TrioqUtil.save(totalEducation)
            user.totalEducation = totalEducation
            TrioqUtil.save(user)
        }
    }

    void createWorkExperience() {
        User user = User.findByUsername("amitsethi0843@gmail.com")
        if (user && user.totalExperience) {
            TotalExperience totalExperience = user.totalExperience

            createExperience().each {
                totalExperience.addToCompanyExperiences(it)
            }
            totalExperience.totalMonths = 36
            TrioqUtil.save(totalExperience)
            user.totalExperience = totalExperience
            TrioqUtil.save(user)
        }
    }


    private List<Experience> createExperience() {
        List<Experience> experienceList = []
        Calendar from = Calendar.getInstance()
        Calendar to = Calendar.getInstance()
        from.set(Calendar.YEAR, 2014)
        from.set(Calendar.MONTH, 2)
        to.set(Calendar.YEAR, 2016)
        to.set(Calendar.MONTH, 4)
        Experience nexthoughts = new Experience(from.getTime(), to.getTime(), "full stack developer", "NexThoughts", false)
        TrioqUtil.save(nexthoughts)
        from.set(Calendar.YEAR, 2016)
        from.set(Calendar.MONTH, 4)
        to.set(Calendar.YEAR, 2016)
        to.set(Calendar.MONTH, 9)
        Experience americanSwan = new Experience(from.getTime(), to.getTime(), "full stack developer", "American Swan", false)
        TrioqUtil.save(americanSwan)
        from.set(Calendar.YEAR, 2016)
        from.set(Calendar.MONTH, 11)
        Experience hcl = new Experience(from.getTime(), "Software Developer", "HCL Technologies", true)
        TrioqUtil.save(hcl)
        experienceList.add(nexthoughts)
        experienceList.add(americanSwan)
        experienceList.add(hcl)
        experienceList
    }

    private List<EducationQualification> createEducationQualification() {
        List<EducationQualification> educationList = []
        Calendar from = Calendar.getInstance()
        Calendar to = Calendar.getInstance()
        from.set(Calendar.YEAR, 2008)
        from.set(Calendar.MONTH, 5)
        to.set(Calendar.YEAR, 2011)
        to.set(Calendar.MONTH, 4)
        Address bcaAddress = new Address("New Delhi", "Delhi", "India", "Rohini Sector 3")
        if (!TrioqUtil.save(bcaAddress))
            println "error    address"
        EducationQualification bca = new EducationQualification(from.getTime(), to.getTime(), "BCA", "Jagannath Insitute Of Management Sciences", bcaAddress)
        TrioqUtil.save(bca)
        from.set(Calendar.YEAR, 2011)
        from.set(Calendar.MONTH, 6)
        to.set(Calendar.YEAR, 2014)
        to.set(Calendar.MONTH, 4)
        Address mcaAddress = new Address("New Delhi", "Delhi", "India", "Madhuban Chown, Rohini")
        TrioqUtil.save(mcaAddress)
        EducationQualification mca = new EducationQualification(from.getTime(), to.getTime(), "MCA", "Rukmini Devi Institute Of Advnaced Studies", mcaAddress)
        TrioqUtil.save(mca)
        educationList.add(bca)
        educationList.add(mca)
        educationList
    }

    private addMenuIdToRole(String roleName, String menuUuid) {
        Role role = Role.findByAuthority(roleName)
        if (role && menuUuid) {
            role.menuIds.add(menuUuid)
            role.save(flush: true)
        }
    }

    private findOrEditRequestMap(String url, List<String> configAttributes) {
        Requestmap requestmap = Requestmap.findByUrl(url)
        if (!requestmap) {
            requestmap = new Requestmap(url: url, configAttribute: configAttributes.join(","), uuid: TrioqUtil.uniqueID)
            requestmap.save(flush: true)
        }
    }

}
