databaseChangeLog = {

    changeSet(author: "amitsethi (generated)", id: "1501792323922-1") {
        createTable(tableName: "address") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "addressPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "area", type: "VARCHAR(255)")

            column(name: "city", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "country", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "current", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "line1", type: "VARCHAR(255)")

            column(name: "line2", type: "VARCHAR(255)")

            column(name: "permanent", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "pin_code", type: "VARCHAR(255)")

            column(name: "state", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-2") {
        createTable(tableName: "company") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "companyPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "address_id", type: "BIGINT")

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "uuid", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-3") {
        createTable(tableName: "contact") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "contactPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "contact_number", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "is_primary", type: "BOOLEAN") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-4") {
        createTable(tableName: "documents") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "documentsPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-5") {
        createTable(tableName: "education_qualification") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "education_qualificationPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "course_name", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "currently_pursuing", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "from_date", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "till_date", type: "datetime")

            column(name: "university_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "uuid", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-6") {
        createTable(tableName: "educational_experience") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "educational_experiencePK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-7") {
        createTable(tableName: "experience") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "experiencePK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "company_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "current", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "from_date", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "position", type: "VARCHAR(255)")

            column(name: "responsibilities", type: "CLOB")

            column(name: "till_date", type: "datetime")

            column(name: "uuid", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-8") {
        createTable(tableName: "extra_curricular") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "extra_curricularPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-9") {
        createTable(tableName: "menu") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "menuPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "code", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "display_name", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "is_active", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "is_deleted", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "last_updated_by", type: "VARCHAR(255)")

            column(name: "parent_menu_id", type: "BIGINT")

            column(name: "url", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "uuid", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-10") {
        createTable(tableName: "project") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "projectPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "company_id", type: "BIGINT")

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "description", type: "CLOB")

            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "months", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "ongoing", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "organization_project", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "technologies_used", type: "VARCHAR(255)")

            column(name: "title", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "url", type: "VARCHAR(255)")

            column(name: "uuid", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-11") {
        createTable(tableName: "requestmap") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "requestmapPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "config_attribute", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "http_method", type: "VARCHAR(255)")

            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "url", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "uuid", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-12") {
        createTable(tableName: "role") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "rolePK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "authority", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-13") {
        createTable(tableName: "role_menu_ids") {
            column(name: "role_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "menu_ids_string", type: "VARCHAR(255)")

            column(name: "menu_ids_idx", type: "INT")
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-14") {
        createTable(tableName: "skill") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "skillPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "description", type: "CLOB")

            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "release_date", type: "datetime")

            column(name: "users_using_it", type: "INT") {
                constraints(nullable: "false")
            }

            column(name: "uuid", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "world_wide_ranking", type: "INT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-15") {
        createTable(tableName: "skill_source_urls") {
            column(name: "skill_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "source_urls_string", type: "VARCHAR(255)")

            column(name: "source_urls_idx", type: "INT")
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-16") {
        createTable(tableName: "social") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "socialPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "profile_link", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "social_network", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-17") {
        createTable(tableName: "supporting_file") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "supporting_filePK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "content_type", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "extension", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "file_type", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "info", type: "VARCHAR(255)")

            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "path", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "user_id", type: "BIGINT")

            column(name: "uuid", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-18") {
        createTable(tableName: "total_education") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "total_educationPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-19") {
        createTable(tableName: "total_education_education_qualification") {
            column(name: "total_education_qualifications_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "education_qualification_id", type: "BIGINT")
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-20") {
        createTable(tableName: "total_experience") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "total_experiencePK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "total_months", type: "BIGINT")

            column(name: "user_id", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-21") {
        createTable(tableName: "total_experience_experience") {
            column(name: "total_experience_company_experiences_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "experience_id", type: "BIGINT")
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-22") {
        createTable(tableName: "university") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "universityPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "address_id", type: "BIGINT")

            column(name: "affiliated_with_id", type: "BIGINT")

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "uuid", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-23") {
        createTable(tableName: "user") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "userPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "about_me", type: "CLOB")

            column(name: "account_expired", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "account_locked", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "display_image_id", type: "BIGINT")

            column(name: "email_verification_uuid", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "email_verified", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "enabled", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "first_name", type: "VARCHAR(255)")

            column(name: "last_name", type: "VARCHAR(255)")

            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "password", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "password_expired", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "total_education_id", type: "BIGINT")

            column(name: "username", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "uuid", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-24") {
        createTable(tableName: "user_address") {
            column(name: "user_addresses_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "address_id", type: "BIGINT")
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-25") {
        createTable(tableName: "user_auth_token") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "user_auth_tokenPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "access_count", type: "INT") {
                constraints(nullable: "false")
            }

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "username", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "usertoken", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-26") {
        createTable(tableName: "user_contact") {
            column(name: "user_contacts_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "contact_id", type: "BIGINT")
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-27") {
        createTable(tableName: "user_extra_curricular") {
            column(name: "user_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "extra_curricular_string", type: "VARCHAR(255)")

            column(name: "extra_curricular_idx", type: "INT")
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-28") {
        createTable(tableName: "user_profile_summary_list") {
            column(name: "user_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "profile_summary_list_string", type: "VARCHAR(255)")

            column(name: "profile_summary_list_idx", type: "INT")
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-29") {
        createTable(tableName: "user_project") {
            column(name: "user_projects_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "project_id", type: "BIGINT")
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-30") {
        createTable(tableName: "user_role") {
            column(name: "user_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "role_id", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-31") {
        createTable(tableName: "user_skill") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "user_skillPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "date_created", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "last_updated", type: "datetime") {
                constraints(nullable: "false")
            }

            column(name: "level", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "skill_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "user_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "uuid", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "experience_in_months", type: "BIGINT")
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-32") {
        createTable(tableName: "user_social") {
            column(name: "user_social_networks_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "social_id", type: "BIGINT")
        }
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-33") {
        addPrimaryKey(columnNames: "user_id, role_id", constraintName: "user_rolePK", tableName: "user_role")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-34") {
        addUniqueConstraint(columnNames: "authority", constraintName: "UC_ROLEAUTHORITY_COL", tableName: "role")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-35") {
        addUniqueConstraint(columnNames: "username", constraintName: "UC_USERUSERNAME_COL", tableName: "user")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-36") {
        addUniqueConstraint(columnNames: "http_method, url", constraintName: "UK3d11b687954e6645e90db4e23cb4", tableName: "requestmap")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-37") {
        addForeignKeyConstraint(baseColumnNames: "social_id", baseTableName: "user_social", constraintName: "FK2imfwepm6jg7oinh3duea4xvd", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "social")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-38") {
        addForeignKeyConstraint(baseColumnNames: "contact_id", baseTableName: "user_contact", constraintName: "FK2pm3m3m7xlwh9nfca4wvgpnwb", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "contact")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-39") {
        addForeignKeyConstraint(baseColumnNames: "display_image_id", baseTableName: "user", constraintName: "FK3oi5avbuhmnoo95amiube59yl", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "supporting_file")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-40") {
        addForeignKeyConstraint(baseColumnNames: "affiliated_with_id", baseTableName: "university", constraintName: "FK3sbj4bw7hk8uoj9ip20nvmnlr", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "university")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-41") {
        addForeignKeyConstraint(baseColumnNames: "experience_id", baseTableName: "total_experience_experience", constraintName: "FK4bejunhhm4owc9hd127thk5pm", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "experience")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-42") {
        addForeignKeyConstraint(baseColumnNames: "user_social_networks_id", baseTableName: "user_social", constraintName: "FK6omrgpjvkttlsd5g8ne6e3wws", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-43") {
        addForeignKeyConstraint(baseColumnNames: "company_id", baseTableName: "project", constraintName: "FK76fngi71pfr8phbobe5pq0swd", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "company")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-44") {
        addForeignKeyConstraint(baseColumnNames: "total_education_id", baseTableName: "user", constraintName: "FK7qupvwqc96it0eyufc95p9ql7", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "total_education")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-45") {
        addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "user_role", constraintName: "FK859n2jvi8ivhui0rl0esws6o", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-46") {
        addForeignKeyConstraint(baseColumnNames: "role_id", baseTableName: "user_role", constraintName: "FKa68196081fvovjhkek5m97n3y", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "role")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-47") {
        addForeignKeyConstraint(baseColumnNames: "education_qualification_id", baseTableName: "total_education_education_qualification", constraintName: "FKaf7i6scfhm2ul1o6l9yl3ve3w", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "education_qualification")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-48") {
        addForeignKeyConstraint(baseColumnNames: "user_addresses_id", baseTableName: "user_address", constraintName: "FKb9t7qdq7dqwk3oossyjewahfr", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-49") {
        addForeignKeyConstraint(baseColumnNames: "address_id", baseTableName: "user_address", constraintName: "FKdaaxogn1ss81gkcsdn05wi6jp", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "address")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-50") {
        addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "supporting_file", constraintName: "FKf0yw6rcrw9me0fna63s5i9vx7", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-51") {
        addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "user_skill", constraintName: "FKfixgsonf2ev168mfck7co17u1", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-52") {
        addForeignKeyConstraint(baseColumnNames: "company_id", baseTableName: "experience", constraintName: "FKfnhfue8bjghnfyjqrxnlgndso", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "company")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-53") {
        addForeignKeyConstraint(baseColumnNames: "user_projects_id", baseTableName: "user_project", constraintName: "FKg600byao3n028lxxntiyuw4c9", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-54") {
        addForeignKeyConstraint(baseColumnNames: "address_id", baseTableName: "company", constraintName: "FKgfifm4874ce6mecwj54wdb3ma", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "address")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-55") {
        addForeignKeyConstraint(baseColumnNames: "parent_menu_id", baseTableName: "menu", constraintName: "FKht6h4dvumr09qxk95r1qcgjd8", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "menu")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-56") {
        addForeignKeyConstraint(baseColumnNames: "university_id", baseTableName: "education_qualification", constraintName: "FKi1m6jrsxtnh8xw6pin234x0is", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "university")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-57") {
        addForeignKeyConstraint(baseColumnNames: "skill_id", baseTableName: "user_skill", constraintName: "FKj53flyds4vknyh8llw5d7jdop", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "skill")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-58") {
        addForeignKeyConstraint(baseColumnNames: "user_contacts_id", baseTableName: "user_contact", constraintName: "FKkc8fuvn775pmfiifm00k2iy4w", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-59") {
        addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "total_experience", constraintName: "FKlqc5t7k1o8v6tasj4juoyothe", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-60") {
        addForeignKeyConstraint(baseColumnNames: "project_id", baseTableName: "user_project", constraintName: "FKocfkr6u2yh3w1qmybs8vxuv1c", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "project")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-61") {
        addForeignKeyConstraint(baseColumnNames: "address_id", baseTableName: "university", constraintName: "FKq9af0t50fvg0i0upumvgrig44", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "address")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-62") {
        addForeignKeyConstraint(baseColumnNames: "total_experience_company_experiences_id", baseTableName: "total_experience_experience", constraintName: "FKqsd80t3q5b4ofn3jrvl5qycvk", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "total_experience")
    }

    changeSet(author: "amitsethi (generated)", id: "1501792323922-63") {
        addForeignKeyConstraint(baseColumnNames: "total_education_qualifications_id", baseTableName: "total_education_education_qualification", constraintName: "FKsr3jybtw4xqdv49ugekjly360", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "total_education")
    }
}
