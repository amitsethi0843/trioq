package com.wyreguide.university

import com.wyreguide.common.Address
import com.wyreguide.util.BaseDomain
import com.wyreguide.util.TrioqUtil

class University extends BaseDomain{

    String name
    Address address
    University affiliatedWith

    static constraints = {
        address nullable: true
        name nullable: false,blank: false
    }

    University(String name,Address address=null){
        this.name=name
        address?this.address=address:""
        this.uuid=TrioqUtil.uniqueID

    }
}
