package com.wyreguide.util

class Menu extends BaseDomain {

    String displayName
    String code
    Menu parentMenu
    String url
    Boolean isDeleted = false
    Boolean isActive = true
    String lastUpdatedBy

    static hasMany = [menuItems:Menu]
    Menu updateMenu(String display,String code,String url){
        this.displayName=display
        this.code=code
        this.url=url
        return this
    }

    static constraints = {
        parentMenu(nullable: true)
        lastUpdatedBy(nullable: true)
    }
}
