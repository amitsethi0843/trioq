package com.wyreguide.social

import com.wyreguide.core.enums.Simple.SocialNetwork
import com.wyreguide.rest.co.UserSocialProfileCO

class Social {

    String profileLink
    SocialNetwork socialNetwork

    static constraints = {
        socialNetwork nullable: false
        profileLink nullable: false
    }

    Social(UserSocialProfileCO userSocialProfileCO){
        this.socialNetwork=SocialNetwork."${userSocialProfileCO.socialAccount}"
        this.profileLink=userSocialProfileCO.socialAccountUrl
    }
}
