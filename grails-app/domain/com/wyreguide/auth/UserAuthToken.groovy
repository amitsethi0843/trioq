package com.wyreguide.auth

class UserAuthToken {

    String usertoken
    String username
    Date dateCreated
    Date lastUpdated
    Integer accessCount = 0

    def afterLoad() {
        accessCount++
    }

}
