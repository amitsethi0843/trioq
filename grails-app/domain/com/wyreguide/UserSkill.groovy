package com.wyreguide

import com.wyreguide.technology.Skill
import com.wyreguide.util.BaseDomain
import com.wyreguide.util.TrioqUtil

class UserSkill extends BaseDomain {

    User user
    Skill skill
    Long level

    UserSkill(User user,Skill skill,Long percentage){
        this.user=user
        this.uuid=TrioqUtil.uniqueID
        this.skill=skill
        this.level=percentage
    }

    static constraints = {

    }
}
