package com.wyreguide

import com.wyreguide.common.Address
import com.wyreguide.common.Contact
import com.wyreguide.common.SupportingFile
import com.wyreguide.education.TotalEducation
import com.wyreguide.experience.TotalExperience
import com.wyreguide.project.Project
import com.wyreguide.rest.co.RegisterUserCO
import com.wyreguide.rest.co.UserProfileCO
import com.wyreguide.social.Social
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode(includes = 'username')
@ToString(includes = 'username', includeNames = true, includePackage = false)
class User implements Serializable {

    private static final long serialVersionUID = 1

    transient springSecurityService

    String username
    String password
    String firstName
    String lastName
    String aboutMe
    SupportingFile displayImage
    TotalExperience totalExperience
    TotalEducation totalEducation
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired
    Date dateCreated
    Date lastUpdated
    List<String> extraCurricular = []
    List<String> profileSummaryList = []
    boolean emailVerified=false
    String emailVerificationUuid= UUID.randomUUID().toString().replaceAll("-","")
    String uuid = UUID.randomUUID().toString().replaceAll("-","")

    User(String username, String password) {
        this()
        this.username = username
        this.password = password
        this.totalExperience = new TotalExperience(user: this)
        this.totalEducation = new TotalEducation(user: this)
    }

    User(String username, String password, String firstName, String lastName) {
        this()
        this.username = username
        this.password = password
        this.firstName = firstName
        this.lastName = lastName
        this.totalExperience = new TotalExperience(user: this)
        this.totalEducation = new TotalEducation(user: this)
    }

    User(RegisterUserCO registerUserCO) {
        this.username = registerUserCO.username
        this.totalExperience = new TotalExperience(user: this)
        this.totalEducation = new TotalEducation(user: this)
    }

    Set<Role> getAuthorities() {
        UserRole.findAllByUser(this)*.role
    }

    Set<UserSkill> getSkills() {
        UserSkill.findAllByUser(this)
    }

    static hasOne = ["totalExperience": TotalExperience]

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
        password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
    }

    static transients = ['springSecurityService', 'skills']


    static hasMany = ['contacts': Contact, 'addresses': Address, 'projects': Project,'socialNetworks':Social]


    static constraints = {
        username blank: false, unique: true
        password blank: false
        firstName nullable: true, blank: true
        totalExperience nullable: true
        totalEducation nullable: true
        displayImage nullable: true
        lastName nullable: true, blank: true
        aboutMe nullable: true, blank: true
        addresses nullable: true
        contacts nullable: true
        projects nullable: true
        socialNetworks nullable: true
    }

    static mapping = {
        password column: '`password`'
        aboutMe type: 'text'
    }

    void createProfile(UserProfileCO userProfileCO) {
        userProfileCO?.firstName ? this.firstName = userProfileCO?.firstName : ""
        userProfileCO?.lastName ? this.lastName = userProfileCO?.lastName : ""
        userProfileCO?.aboutMe ? this.aboutMe = userProfileCO?.aboutMe : ""
        userProfileCO?.profileSummary ? this.profileSummaryList = userProfileCO?.profileSummary?.split(";") : ""
        Address address = this.addresses ? this.addresses.find { it.current } : new Address()
        address.initialize(userProfileCO)
        this.addToAddresses(address)
//        if (userProfileCO?.totalExperience) {
//
//        }
    }

}
