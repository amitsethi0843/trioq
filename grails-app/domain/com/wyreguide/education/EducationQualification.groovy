package com.wyreguide.education

import com.wyreguide.common.Address
import com.wyreguide.rest.co.UserEducationCO
import com.wyreguide.university.University
import com.wyreguide.util.BaseDomain
import com.wyreguide.util.TrioqUtil

class EducationQualification extends BaseDomain implements Comparable<EducationQualification>{

    boolean currentlyPursuing=false
    Date fromDate
    Date tillDate
    String courseName
    University university

    static constraints = {
        fromDate nullable: false
        tillDate nullable: true
        courseName nullable: false
    }

    @Override
    public int compareTo(EducationQualification o) {
        return o.fromDate.compareTo(this.fromDate)
    }

    EducationQualification(Date from, Date till, String courseName, String universityName, Address uniAddress=null){
        University university=University.findByName(universityName)
        if(!university){
            university=new University(universityName,uniAddress)
            TrioqUtil.save(university);
        }
        this.university=university
        this.uuid=TrioqUtil.uniqueID
        this.fromDate=from
        this.tillDate=till
        this.courseName=courseName
    }

    EducationQualification(UserEducationCO userEducationCO){
        University university=University.findByName(userEducationCO.university)
        if(!university){
            university=new University(userEducationCO.university)
            TrioqUtil.save(university);
        }
        this.university=university
        this.uuid=TrioqUtil.uniqueID
        this.fromDate=TrioqUtil.strToDate(userEducationCO.fromDate)
        if (userEducationCO.currentlyPursuing.equals("true")) {
            println("==================="+currentlyPursuing)
            this.currentlyPursuing=true
            this.tillDate = null
        } else {
            userEducationCO.tillDate ? this.tillDate = TrioqUtil.strToDate(userEducationCO.tillDate) : ""
        }
        this.courseName=userEducationCO.courseName
    }


    void update(UserEducationCO userEducationCO) {
        if (this.university &&
                this.university.name.replaceAll("\\s", "").toLowerCase().equals(userEducationCO.university.replaceAll("\\s", "").toLowerCase())) {
            this.university.name = userEducationCO?.university
        } else {
            this.university = University.findByName(userEducationCO.university)
            if(!university){
                university= (University) TrioqUtil.saveObject(new University(userEducationCO.university))
            }
        }
        this.courseName=userEducationCO?.courseName?:""
        this.fromDate=TrioqUtil.strToDate(userEducationCO.fromDate)
        if (userEducationCO.currentlyPursuing.equals("true")) {
            this.currentlyPursuing=true
            this.tillDate = null
        } else {
            userEducationCO.tillDate ? this.tillDate = TrioqUtil.strToDate(userEducationCO.tillDate) : ""
        }
    }
}
