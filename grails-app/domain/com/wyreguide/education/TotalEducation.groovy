package com.wyreguide.education

import com.wyreguide.User

class TotalEducation {

    static hasMany = ['qualifications':EducationQualification]
    static belongsTo = ['user':User]

    static constraints = {
    }
}
