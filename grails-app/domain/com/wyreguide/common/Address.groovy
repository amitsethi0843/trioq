package com.wyreguide.common

import com.wyreguide.rest.co.UserProfileCO

class Address {

    String line1
    String line2
    String area
    String city
    String state
    String country
    String pinCode
    boolean permanent=false
    boolean current=false

    static constraints = {
        line1(nullable: true, blank: true)
        line2(nullable: true, blank: true)
        area(nullable: true, blank: true)
        city(nullable: false, blank: false)
        state(nullable: false, blank: false)
        country(nullable: false, blank: false)
        pinCode(nullable: true, blank: true)
    }

    Address(String city,String state,String country,String line1=null,String line2=null, String area=null,String pincode=null){
        this.city=city
        this.state=state
        this.country=country
        this.pinCode=pinCode
        this.line1=line1
        this.line2=line2
        this.area=area
    }

    void initialize(UserProfileCO userProfileCO){
        this.city=userProfileCO?.city
        this.country=userProfileCO?.country
        this.state=userProfileCO?.state
        userProfileCO.line1 ? this.line1=userProfileCO.line1 : ""
        userProfileCO.line2 ? this.line2=userProfileCO.line2 : ""
        this.current=true
    }
}
