package com.wyreguide.common

import com.wyreguide.User
import com.wyreguide.core.enums.Simple.FileType
import com.wyreguide.util.BaseDomain
import com.wyreguide.util.TrioqUtil
import com.wyreguide.util.FileUtil

import javax.servlet.http.Part

class SupportingFile extends BaseDomain {

    String name
    String path
    String info
    String contentType
    String extension
    FileType fileType
    User user

    static constraints = {
        user nullable: true
        info nullable: true, blank: true
        contentType nullable: false,blank: false
        extension nullable: false,blank: false
        fileType nullable: false
        path nullable: false
    }

    SupportingFile(Part partFile, String path, FileType fileType, User user,String name=null) {
        this.name = name?:partFile.name
        this.uuid = TrioqUtil.uniqueID
        this.fileType = fileType
        this.extension = FileUtil.getExtension(partFile)
        this.contentType=partFile.contentType
        this.path = path + "/${this.getName()}"
        this.user = user
    }
}
