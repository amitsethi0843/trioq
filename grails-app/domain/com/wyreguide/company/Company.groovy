package com.wyreguide.company

import com.wyreguide.common.Address
import com.wyreguide.util.BaseDomain
import com.wyreguide.util.TrioqUtil

class Company extends BaseDomain {

    String name
    Address address

    static constraints = {
        name nullable: false,blank: false
        address nullable: true
    }

    Company(String name){
        this.name=name
        this.uuid=TrioqUtil.uniqueID
    }
}
