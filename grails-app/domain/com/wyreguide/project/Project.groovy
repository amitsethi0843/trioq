package com.wyreguide.project

import com.wyreguide.common.SupportingFile
import com.wyreguide.company.Company
import com.wyreguide.rest.co.UserProjectCO
import com.wyreguide.util.BaseDomain
import com.wyreguide.util.TrioqUtil
import grails.dev.Support

class Project extends BaseDomain {
    String title
    String url
    String technologiesUsed
    boolean organizationProject = true
    Company company
    long months=0
    String description
    boolean ongoing=false

    static mapping = {
        description type: 'text'
    }

    static constraints = {
        title nullable: false, blank: false
        url nullable: true, blank: true
        company nullable: true
        technologiesUsed nullable: true
        description nullable: true, blank: true
    }

    Project(String title, boolean organizationProject,long months,boolean ongoing, String url = "",  Company company = null, String description = "",String technologies=null) {
        this.title = title
        url ? this.url = url : ""
        company ? this.company = company : ""
        this.uuid=TrioqUtil.uniqueID;
        description ? this.description = description : ""
        this.organizationProject=organizationProject
        this.ongoing=ongoing
        this.months=months
        technologies?this.technologiesUsed=technologies:""
    }

    Project(UserProjectCO userProjectCO){
        this.title=userProjectCO?.title
        this.uuid=TrioqUtil.uniqueID;
        this.url=userProjectCO?.url
        this.organizationProject=userProjectCO?.organizationProject
        if(userProjectCO?.organizationProject){
            this.company=new Company(userProjectCO?.organization)
        }
        this.ongoing=userProjectCO?.ongoing
        this.months=userProjectCO?.months ? Long.parseLong(userProjectCO?.months) : null
        userProjectCO?.technologiesUsed ?this.technologiesUsed=userProjectCO?.technologiesUsed:""
        userProjectCO?.description ? this.description=userProjectCO?.description : ""
    }

    void update(UserProjectCO userProjectCO){
        this.title=userProjectCO?.title
        this.url=userProjectCO?.url
        this.organizationProject=userProjectCO?.organizationProject
        if(userProjectCO?.organizationProject){
            this.company=new Company(userProjectCO?.organization)
        }
        this.ongoing=userProjectCO?.ongoing
        this.months=userProjectCO?.months ? Long.parseLong(userProjectCO?.months) : null
        this.technologiesUsed=userProjectCO?.technologiesUsed
        this.description=userProjectCO?.description
    }
}
