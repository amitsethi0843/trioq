package com.wyreguide.technology

import com.wyreguide.util.BaseDomain
import com.wyreguide.util.TrioqUtil

class Skill extends BaseDomain {

    String name
    String description

    Date releaseDate

    int usersUsingIt=0

    int worldWideRanking = 0
    List<String> sourceUrls = []

    static mapping = {
        description type: 'text'
    }


    static constraints = {
        name nullable: false, blank: false
        description nullable: true, blank: true
        releaseDate nullable: true
    }

    Skill(String name, String description, Date releaseDate = null, List<String> sourceUrls = []) {
        this.name = name
        this.description = description
        this.name = name
        this.usersUsingIt+=1
        this.uuid=TrioqUtil.uniqueID
        releaseDate ? this.releaseDate = releaseDate : ''
        sourceUrls ? this.sourceUrls = sourceUrls : ""
    }




}
