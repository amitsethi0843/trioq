package com.wyreguide.experience

import com.wyreguide.User
import com.wyreguide.technology.Skill

class TotalExperience {

    Long totalMonths

    static hasMany = ["companyExperiences":Experience]

    static belongsTo = ["user":User]

    static constraints = {
        totalMonths  nullable: true
    }
}
