package com.wyreguide.experience

import com.wyreguide.company.Company
import com.wyreguide.rest.co.UserExperienceCO
import com.wyreguide.util.BaseDomain
import com.wyreguide.util.TrioqUtil


class Experience extends BaseDomain implements Comparable<Experience> {

    boolean current = false
    Date fromDate
    Date tillDate
    String position
    String responsibilities
    Company company
    static mapping = {
        responsibilities type: "text"
    }

    Experience(Date fromDate, Date till, String position, String companyName, Boolean current = false) {
        this.fromDate = fromDate
        this.tillDate = till
        this.position = position
        this.uuid = TrioqUtil.uniqueID
        Company company = new Company(name: companyName, uuid: TrioqUtil.uniqueID)
        TrioqUtil.save(company)
        this.company = company
        this.current = current
    }

    Experience(Date fromDate, String position, String companyName, Boolean current = false) {
        this.fromDate = fromDate
        this.position = position
        this.uuid = TrioqUtil.uniqueID
        Company company = new Company(name: companyName, uuid: TrioqUtil.uniqueID)
        TrioqUtil.save(company)
        this.company = company
        this.current = current
    }

    Experience(UserExperienceCO userExperienceCO) {
        if (this.company &&
                this.company.name.replaceAll("\\s", "").toLowerCase().equals(userExperienceCO.companyName.replaceAll("\\s", "").toLowerCase())) {
            this.company.name = userExperienceCO?.companyName
        } else {
            this.company = (Company) TrioqUtil.saveObject(new Company(userExperienceCO.companyName))
        }
        this.position = userExperienceCO.role
        this.uuid=TrioqUtil.uniqueID
        this.fromDate = TrioqUtil.strToDate(userExperienceCO.fromDate)
        if (userExperienceCO.presentCompany.equals("true")) {
            this.tillDate = null
        } else {
            userExperienceCO?.tillDate ? this.tillDate = TrioqUtil.strToDate(userExperienceCO.tillDate) : ""
        }
        userExperienceCO?.responsibilities ? this.responsibilities = userExperienceCO.responsibilities : ""
    }

    void update(UserExperienceCO userExperienceCO) {
        if (this.company &&
                this.company.name.replaceAll("\\s", "").toLowerCase().equals(userExperienceCO.companyName.replaceAll("\\s", "").toLowerCase())) {
            this.company.name = userExperienceCO?.companyName
        } else {
            this.company = (Company) TrioqUtil.saveObject(new Company(userExperienceCO.companyName))
        }
        this.position = userExperienceCO.role
        this.fromDate = TrioqUtil.strToDate(userExperienceCO.fromDate)
        if (userExperienceCO.presentCompany.equals("true")) {
            this.current=true
            this.tillDate = null
        } else {
            userExperienceCO?.tillDate ? this.tillDate = TrioqUtil.strToDate(userExperienceCO.tillDate) : ""
        }
        userExperienceCO?.responsibilities ? this.responsibilities = userExperienceCO.responsibilities : ""
    }

    @Override
    public int compareTo(Experience o) {
        return o.fromDate.compareTo(this.fromDate)
    }

    static constraints = {
        fromDate nullable: false
        tillDate nullable: true
        position nullable: true, blank: true
        responsibilities nullable: true, blank: true
    }
}
