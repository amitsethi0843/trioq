package wyreguide

import com.wyreguide.Role
import com.wyreguide.User
import com.wyreguide.admin.MenuService
import com.wyreguide.util.Menu

class AdminTagLib {
    static namespace = "wgAdmin"

    MenuService menuService
    def springSecurityService
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    def showMenu = { attrs, body ->
        String menuCode = attrs.menu
        boolean isRoleAssigned = false
        User user = springSecurityService.currentUser
        if (user && menuCode) {
            Menu menu = Menu.findByCode(menuCode)
            List<Role> roleList = user.authorities.toList()
            if (roleList) {
                for (Role role : roleList) {
                    if (role?.menuIds?.contains(menu?.uuid)) {
                        isRoleAssigned = true
                    }
                }
            }

        }
        if(isRoleAssigned)
        out << body()
    }
}
