package com.wyreguide


class RestInterceptor {

    def springSecurityService

    RestInterceptor() {
        match(uri: '/api/**')
    }

    boolean before() {
        println("=============" + new Date() + "=========" + params)
        true
    }

    boolean after() { true }

    void afterView() {
        // no-op
    }
}
