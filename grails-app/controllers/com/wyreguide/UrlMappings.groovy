package com.wyreguide

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: 'admin',action: 'adminEntryPoint')
        name adminHome:"/admin/"(controller: 'admin',action: 'adminHomePage')
        name visitorView:"/visitor/lits"(controller: 'adminVisitor',action: 'list')
        "/api/register"(controller: 'user',action: 'registerUser',method:'POST')
        "/api/userProfile/$username"(controller: 'userRest',action: 'profile')
        "/api/userProfile/resume/$userUuid"(controller: 'userRest',action: 'resume')
        "/api/userProfile/contactAdmin"(controller: 'userRest',action: 'contactAdmin',method:'POST')
        "/api/userProfile/contactUser"(controller: 'userRest',action: 'contactUser',method:'POST')
        "/api/manageProfile/basicDetails"(controller: 'userProfile',action: 'updateBasicDetails',method:'POST')
        "/api/manageProfile/fetchProfile"(controller: 'userProfile',action: 'retrieveProfile',method:'GET')
        "/api/manageProfile/experience"(controller: 'userProfile',action: 'createExperience',method:'POST')
        "/api/manageProfile/experience"(controller: 'userProfile',action: 'deleteExperience',method:'DELETE')
        "/api/manageProfile/project"(controller: 'userProfile',action: 'createProject',method:'POST')
        "/api/manageProfile/project"(controller: 'userProfile',action: 'removeProject',method:'DELETE')
        "/api/manageProfile/education"(controller: 'education',action: 'create',method:'POST')
        "/api/manageProfile/education"(controller: 'education',action: 'delete',method:'DELETE')
        "/api/manageProfile/populateSkills"(controller: 'skill',action: 'populateSkillDropdown',method:'POST')
        "/api/manageProfile/skills"(controller: 'skill',action: 'save',method:'POST')
        "/api/manageProfile/skills"(controller: 'skill',action: 'delete',method:'DELETE')
        "/api/manageProfile/socialProfile"(controller: 'social',action: 'save',method:'POST')
        "/api/manageProfile/socialProfile"(controller: 'social',action: 'retrieve',method:'GET')
        "/api/manageProfile/profileImage"(controller: 'userProfile',action: 'saveProfileImage',method:'POST')
        "/api/manageProfile/resume"(controller: 'userProfile',action: 'saveResume',method:'POST')
        "/api/verifyEmail"(controller: 'userRest',action: 'verifyEmail',method:'GET')



        "500"(view:'/error')
        "404"(view:'/notFound')

    }
}
