package com.wyreguide

import com.wyreguide.core.CacheManagementService
import com.wyreguide.core.EMailService
import com.wyreguide.core.exception.TrioqException
import grails.converters.JSON

class BaseController {
    EMailService EMailService
    CacheManagementService cacheManagementService
    def test(){
//        def userCache=cacheManagementService.getUserCache()
//        println(userCache)
        EMailService.sendRegistrationEmail(User.findByUsername("amitsethi0843@gmail.com"),TrioqException.createBodyWithSuccessErrorMap())
        render([notApproved: "Your details are not verified"] as JSON)
    }

}
