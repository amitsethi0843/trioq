package com.wyreguide


class AdminInterceptor {

    def springSecurityService

    AdminInterceptor() {
        match(uri: '/admin/**')
    }

    boolean before() {
        String uri = request.getRequestURI()
        User user = springSecurityService.currentUser
        println("----------------uri----"+uri)
        println("----------------auth----"+user?user?.authorities*.authority:"")
        true
    }

    boolean after() { true }

    void afterView() {
        // no-op
    }
}
