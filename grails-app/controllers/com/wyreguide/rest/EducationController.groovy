package com.wyreguide.rest

import com.wyreguide.User
import com.wyreguide.rest.co.UserEducationCO

class EducationController extends BaseController{

    EducationService educationService;

    def create(UserEducationCO userEducationCO){
        println(userEducationCO.currentlyPursuing)
        Closure closure={
            User user=getUser()
            educationService.saveEducation(user,userEducationCO)
        }
        generateResponse closure
    }

    def retrieve(){

    }

    def update(){

    }

    def delete(){
        def response = {
            String uuid=request.JSON.uuid
            User user = getUser()
            educationService.removeEducation(user,uuid)
        }
        generateResponse response
    }
}
