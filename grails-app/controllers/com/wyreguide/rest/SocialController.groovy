package com.wyreguide.rest

import com.wyreguide.User
import com.wyreguide.rest.co.UserSocialProfileCO

class SocialController extends BaseController{

    SocialService socialService;

    def save(UserSocialProfileCO userSocialProfileCO){
        Closure profiles={
            User user=getUser()
            socialService.save(userSocialProfileCO,user)
        }
        generateResponse  profiles
    }

    def retrieve(){
        Closure profiles={
            User user=getUser()
            socialService.retrieve(user)
        }
        generateResponse  profiles
    }
}
