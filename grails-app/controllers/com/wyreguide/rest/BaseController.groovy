package com.wyreguide.rest

import com.wyreguide.User
import com.wyreguide.rest.dto.APIResponseDTO
import com.wyreguide.rest.exception.UserNotLogedInException
import com.wyreguide.rest.exception.ValidationException
import grails.converters.JSON
import org.springframework.context.MessageSource

import javax.servlet.http.HttpServletRequest
import javax.validation.Valid
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


class BaseController {

    def springSecurityService
    MessageSource messageSource

    protected void generateResponse(Closure closure) {
        APIResponseDTO dto = new APIResponseDTO()
        def responseData = [:]
        response.setHeader('Cache-Control', 'no-cache, no-store')
        try {
            Object object = closure()
            dto = (APIResponseDTO) object
            response.status = dto.statusCode
            responseData.data = dto.data
//            log.debug("ApiResponseDto content : ${dto as JSON}}")
        }
        catch (UserNotLogedInException ex1) {
            dto.setInternalServerErrorResponse(ex1, HttpURLConnection.HTTP_UNAUTHORIZED)
        }
        catch (ValidationException e) {
            dto.setInternalServerErrorResponse(e, HttpURLConnection.HTTP_BAD_REQUEST)
            println(e)
        }
        catch (Exception e) {
            e.printStackTrace()
            print(e)
            log.error("Exception caught ::", e)
            dto.setInternalServerErrorResponse(e)
//            response.status = HttpURLConnection.HTTP_INTERNAL_ERROR
        }
        finally {
            responseData.putAll([status: dto.statusCode,errorCode:dto?.code?:200,success:dto?.status, message: dto.message])
            renderResponse(responseData)
        }
    }


    private def renderResponse(Map responseMap) {
        render responseMap as JSON
    }

    protected String extractAuthToken(HttpServletRequest request) {
        String authToken = request.getHeader("Authorization")
        /*if (authToken) {
            log.info("Auth Token Found : ${authToken}")
        } else {
            log.info("Auth Token Not Found : Anonymous User")
        }*/
        authToken
    }

    protected def getUser() {
        User user
        String username = springSecurityService.principal ? springSecurityService?.principal?.username : null
        if (username) {
            user = User.findByUsername(username) ?: null
        }
        if (user == null) {
            throw new UserNotLogedInException(messageSource.getMessage("user.not.loggedIn", null, Locale.default))
        }
        return user
    }

}
