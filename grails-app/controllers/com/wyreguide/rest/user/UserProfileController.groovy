package com.wyreguide.rest.user

import com.wyreguide.User
import com.wyreguide.rest.BaseController
import com.wyreguide.rest.UserProfileRestService
import com.wyreguide.rest.co.UserExperienceCO
import com.wyreguide.rest.co.UserProfileCO
import com.wyreguide.rest.co.UserProjectCO
import grails.converters.JSON
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.commons.CommonsMultipartFile

import javax.servlet.http.Part

class UserProfileController extends BaseController {

    UserProfileRestService userProfileRestService

    def updateBasicDetails(UserProfileCO userProfileCO) {
        User user = getUser()
        def response = { userProfileRestService.manageUserProfile(userProfileCO, user) }
        generateResponse response
    }

    def createExperience(UserExperienceCO userExperienceCO){
        def response = {
            User user = getUser()
            userProfileRestService.createUserExperience(userExperienceCO, user)
        }
        generateResponse response
    }

    def createProject(UserProjectCO userProjectCO){
        def response = {
            User user = getUser()
            userProfileRestService.createUserProject(userProjectCO, user)
        }
        generateResponse response
    }

    def removeProject(){
        def response = {
            String uuid=request.JSON.uuid
            User user = getUser()
            userProfileRestService.removeUserProject(user,uuid)
        }
        generateResponse response
    }

    def deleteExperience(){

        def response = {
            String uuid=request.JSON.uuid
            User user = getUser()
            userProfileRestService.removeUserExperience(user,uuid)
        }
        generateResponse response
    }

    def saveProfileImage(){
        Part image=request.getParts()[0]
        User user = getUser()
        def response = { userProfileRestService.saveProfileImage(image, user) }
        generateResponse response
    }

    def saveResume(){
        Part resume=request.getParts()[0]
        User user = getUser()
        def response = { userProfileRestService.saveUserResume(resume, user) }
        generateResponse response
    }

    def retrieveProfile() {
        def response = {
            User user = getUser()
            userProfileRestService.getUserProfile(user)
        }
        generateResponse response
    }



}
