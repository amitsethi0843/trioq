package com.wyreguide.rest.user

import com.wyreguide.rest.BaseController
import com.wyreguide.rest.UserService
import com.wyreguide.rest.co.RegisterUserCO

class UserController extends BaseController {

    UserService userService;

    def registerUser(RegisterUserCO registerUserCO) {
        Closure closure={
            userService.registerUser(registerUserCO)
        }
        generateResponse closure
    }
}
