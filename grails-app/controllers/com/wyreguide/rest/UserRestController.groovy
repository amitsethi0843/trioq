package com.wyreguide.rest

import com.wyreguide.User
import com.wyreguide.rest.co.ContactUserCO

class UserRestController extends BaseController {

    UserProfileRestService userProfileRestService

    def profile(){
        def userProfile={
            userProfileRestService.userProfile(params?.username)
        }
        generateResponse userProfile
    }

    def verifyEmail(){
        def userProfile={
            userProfileRestService.verifyEmail(params?.uuid,params?.username)
        }
        generateResponse userProfile
    }

    def resume(){
        File file = userProfileRestService.resume(params?.userUuid)
        if(file.exists()){
            response.setContentType("application/octet-stream")
            response.setContentLength((int) file.length());
            response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"")
            response.outputStream << file.bytes
            response.outputStream.flush()
        }

    }

    def contactAdmin(ContactUserCO contactUserCO){
        def response={
            userProfileRestService.contactAdmin(contactUserCO)
        }
        generateResponse response
    }

    def contactUser(ContactUserCO contactUserCO){
        def response={
            userProfileRestService.contactUser(contactUserCO)
        }
        generateResponse response
    }


}
