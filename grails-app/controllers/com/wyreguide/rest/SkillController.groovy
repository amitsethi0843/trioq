package com.wyreguide.rest

import com.wyreguide.User
import com.wyreguide.rest.co.UserSkillsCO
import com.wyreguide.rest.dto.APIResponseDTO

class SkillController extends BaseController{

    SkillService skillService

    def populateSkillDropdown(){
        Closure skills={
            String keyword=request.JSON.keyword
            skillService.populateSkillSuggestion(keyword)
        }
        generateResponse  skills
    }

    def save(UserSkillsCO userSkillsCO){
        Closure skills={
            User user=getUser()
            skillService.createUserSkills(user,userSkillsCO)
        }
        generateResponse  skills
    }

    def delete(){
        Closure skills={
            String uuid=request.JSON.uuid
            User user=getUser()
            skillService.removeUserSkill(uuid,user)
        }
        generateResponse  skills
    }
}
