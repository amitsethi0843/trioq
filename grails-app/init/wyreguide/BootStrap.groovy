package wyreguide

import com.wyreguide.BootstrapService
import com.wyreguide.User
import com.wyreguide.core.CacheManagementService

class BootStrap {

    BootstrapService bootstrapService
    CacheManagementService cacheManagementService

    def init = { servletContext ->
        try {
            if (!User.findByUsername("amitsethi0843@gmail.com")) {
                bootstrapService.insertMenu()
                bootstrapService.createAdmin()
                bootstrapService.createRequestMap()
                bootstrapService.createWorkExperience()
                bootstrapService.createTechnology()
                bootstrapService.createQualification()
                bootstrapService.createProjects()
            }
//            cacheManagementService.createCache()
        }
        catch (Exception e) {
            println(e)
        }
    }
    def destroy = {
    }
}
