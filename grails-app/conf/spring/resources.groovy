import com.wyreguide.ApplicationContextHolder
import com.wyreguide.core.eventListeners.AuthenticationSuccessEventListener
import com.wyreguide.util.TrioqUtil
import org.springframework.web.multipart.commons.CommonsMultipartResolver

beans = {
    authenticationSuccessEventListener(AuthenticationSuccessEventListener)
    applicationContextHolder(ApplicationContextHolder) { bean ->
        bean.factoryMethod = 'getInstance'
    }

//    trioqUtil(TrioqUtil) {
//        messageSource = ref('messageSource')
//    }

    multipartResolver(CommonsMultipartResolver){
        maxUploadSize=20971520
        defaultEncoding="UTF-8"
    }
}
