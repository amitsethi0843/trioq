

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'com.wyreguide.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'com.wyreguide.UserRole'
grails.plugin.springsecurity.authority.className = 'com.wyreguide.Role'
grails.plugin.springsecurity.securityConfigType = "Requestmap"
grails.plugin.springsecurity.requestMap.className = 'com.wyreguide.Requestmap'
grails.plugin.springsecurity.requestMap.urlField = 'url'
grails.plugin.springsecurity.requestMap.configAttributeField = 'configAttribute'
grails.plugin.springsecurity.requestMap.httpMethodField = 'httpMethod'
grails.plugin.springsecurity.logout.postOnly = false
grails.plugin.springsecurity.successHandler.defaultTargetUrl = '/admin/'

grails.plugin.springsecurity.useSecurityEventListener = true



grails.plugin.springsecurity.rest.token.storage.useGorm=true
grails.plugin.springsecurity.rest.token.storage.gorm.tokenDomainClassName='com.wyreguide.auth.UserAuthToken'
grails.plugin.springsecurity.rest.token.storage.gorm.tokenValuePropertyName='usertoken'
grails.plugin.springsecurity.rest.token.storage.gorm.usernamePropertyName='username'
grails.plugin.springsecurity.rest.token.generation.useSecureRandom=false
grails.plugin.springsecurity.rest.token.generation.useUUID=true


//grails.plugin.springsecurity.controllerAnnotations.staticRules = [
//	[pattern: '/',               access: ['permitAll']],
//	[pattern: '/error',          access: ['permitAll']],
//	[pattern: '/login',          access: ['permitAll']],
//	[pattern: '/login.*',          access: ['permitAll']],
//	[pattern: '/login/*',          access: ['permitAll']],
//	[pattern: '/logout',          access: ['permitAll']],
//	[pattern: '/logout.*',          access: ['permitAll']],
//	[pattern: '/logout/*',          access: ['permitAll']],
//	[pattern: '/index',          access: ['permitAll']],
//	[pattern: '/index.gsp',      access: ['permitAll']],
//	[pattern: '/shutdown',       access: ['permitAll']],
//	[pattern: '/assets/**',      access: ['permitAll']],
//	[pattern: '/**/js/**',       access: ['permitAll']],
//	[pattern: '/**/css/**',      access: ['permitAll']],
//	[pattern: '/**/images/**',   access: ['permitAll']],
//	[pattern: '/**/favicon.ico', access: ['permitAll']]
//]

grails.plugin.springsecurity.filterChain.chainMap = [
	[pattern: '/assets/**',      filters: 'none'],
	[pattern: '/**/js/**',       filters: 'none'],
	[pattern: '/**/css/**',      filters: 'none'],
	[pattern: '/**/images/**',   filters: 'none'],
	[pattern: '/**/favicon.ico', filters: 'none'],
	[pattern: '/api/userProfile/**',  filters: 'anonymousAuthenticationFilter,restExceptionTranslationFilter,' +
			                              'filterInvocationInterceptor'],
	[pattern: '/api/verifyEmail**',  filters: 'anonymousAuthenticationFilter,restExceptionTranslationFilter,' +
			'filterInvocationInterceptor'],
	[pattern: '/api/register',  filters: 'anonymousAuthenticationFilter,restExceptionTranslationFilter,' +
			'filterInvocationInterceptor'],

	[pattern:'/api/**',          filters: 'JOINED_FILTERS,-anonymousAuthenticationFilter,-exceptionTranslationFilter,' +
										  '-authenticationProcessingFilter,-securityContextPersistenceFilter,-rememberMeAuthenticationFilter'],
	[pattern: '/**',             filters: 'JOINED_FILTERS,-restTokenValidationFilter,-restExceptionTranslationFilter'] ,

	[pattern: '/oauth/**',       filters: 'JOINED_FILTERS,-restTokenValidationFilter,-restExceptionTranslationFilter']
]

grails.default.interceptUrls = ['/**', '/index', '/index.gsp', '/**/favicon.ico','/base/**',
								'/**/js/**', '/**/css/**', '/**/images/**', '/login', '/login.*', '/login/*',
								'/logout', '/logout.*', '/logout/*', '/assets/**', '/util/**', '/upload/**', '/**/fonts/**',
								'/versionInfo.txt']


dataSource {
	pooled = true
	driverClassName = "com.mysql.jdbc.Driver"
	dialect = 'org.hibernate.dialect.MySQL5InnoDBDialect'
}

grails.plugin.aws.credentials.accessKey='eN13Xagwp4pfcaENhnB6jC6ux2744UDEVZ76E0qe'
grails.plugin.aws.credentials.secretKey='AKIAIEPJSYRYEMQG2XMQ'
grails.plugin.aws.s3.bucket=wyrelist-prod



environments {
	development {

		frontEnd.serverUrl="http://localhost:4200"
		grails.serverUrl="http://localhost:8080"
		dataSource {
			dbCreate = "update"
			url = "jdbc:mysql://localhost:3306/wyreagent?autoreconnect=true"
			username = "root"
			password = "technophile2"
		}
	}
	production {

		frontEnd.serverUrl="http://www.trioq.com"
		grails.serverUrl="http://www.api.trioq.com"
		dataSource {
			username = "root"
			password = "technophile2"
			dbCreate = "none"
			url = "jdbc:mysql://localhost:3306/trioq?autoreconnect=true"
			pooled = true
			properties {
				jmxEnabled = true
				initialSize = 5
				maxActive = 50
				minIdle = 5
				maxIdle = 25
				maxWait = 10000
				maxAge = 10 * 60000
				timeBetweenEvictionRunsMillis = 5000
				minEvictableIdleTimeMillis = 60000
				validationQuery = "SELECT 1"
				validationQueryTimeout = 3
				validationInterval = 15000
				testOnBorrow = true
				testWhileIdle = true
				testOnReturn = false
				jdbcInterceptors = "ConnectionState;StatementCache(max=200)"
				defaultTransactionIsolation = java.sql.Connection.TRANSACTION_READ_COMMITTED
			}
		}

	}
}

grails.plugin.databasemigration.updateOnStart = true
grails.plugin.databasemigration.updateOnStartFileName = 'changelog.groovy'

grails.admin.console.interceptUrl = '/console/**'
grails.admin.console.plugin.interceptUrl = '/plugins/console*/**'
grails.authenticated.adminHome.interceptUrl = '/admin/**'

grails.controllers.upload.maxFileSize=10000000000000
grails.controllers.upload.maxRequestSize=10000000000000

google.cloud.bucket='dappled-genre-5741'
google.storage.path='trioq'

grails {
	mail {
		host = "smtp.zoho.com"
		port = "587"
		username = "amit@trioq.com"
		password = "technophile2"
		props = ["mail.smtp.auth":"true",
				 "mail.smtp.starttls.enable": "true",
				 "mail.smtp.socketFactory.port":"587",
				 "mail.smtp.socketFactory.fallback":"false",
				 "mail.smtps.quitwait": "false"]
	}
}

postmarkApi.token='a0b6bf59-4005-4403-a8b1-0623a6a46b5c'
postmarkApi.url='https://api.postmarkapp.com/email'

trioq.supportEmail='support@trioq.com'
trioq.adminEmail='admin@trioq.com'
